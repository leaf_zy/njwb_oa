/*
 Navicat Premium Data Transfer

 Source Server         : mysql@localhost-3306
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost
 Source Database       : njwb_oa

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : utf-8

 Date: 02/27/2022 00:31:21 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `error`
-- ----------------------------
DROP TABLE IF EXISTS `error`;
CREATE TABLE `error` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `error`
-- ----------------------------
BEGIN;
INSERT INTO `error` VALUES ('1', 'aaa'), ('2', 'bbb'), ('3', 'ccc');
COMMIT;

-- ----------------------------
--  Table structure for `t_bank_card`
-- ----------------------------
DROP TABLE IF EXISTS `t_bank_card`;
CREATE TABLE `t_bank_card` (
  `b_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_card_no` varchar(20) NOT NULL,
  `b_card_money` int(11) NOT NULL,
  `b_stu_id` int(11) NOT NULL,
  PRIMARY KEY (`b_card_id`),
  KEY `fk_stu_id` (`b_stu_id`),
  CONSTRAINT `fk_stu_id` FOREIGN KEY (`b_stu_id`) REFERENCES `t_student` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_bank_card`
-- ----------------------------
BEGIN;
INSERT INTO `t_bank_card` VALUES ('1', 'bankno1', '500', '1'), ('2', 'bankno2', '200', '2'), ('3', 'bankno3', '1000', '2'), ('4', 'bankno4', '400', '1');
COMMIT;

-- ----------------------------
--  Table structure for `t_card`
-- ----------------------------
DROP TABLE IF EXISTS `t_card`;
CREATE TABLE `t_card` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_type` varchar(20) NOT NULL,
  `card_state` varchar(20) NOT NULL,
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_card`
-- ----------------------------
BEGIN;
INSERT INTO `t_card` VALUES ('1', '普通用户卡', '有效'), ('2', 'VIP卡', '欠费');
COMMIT;

-- ----------------------------
--  Table structure for `t_dept`
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_no` varchar(5) NOT NULL,
  `dept_name` varchar(20) NOT NULL,
  `dept_location` varchar(20) NOT NULL,
  `dept_pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_dept`
-- ----------------------------
BEGIN;
INSERT INTO `t_dept` VALUES ('1', 'no1', 'name1', 'loc1', '6'), ('2', 'no2', 'name2', 'loc2', '6'), ('3', 'no3', 'name3', 'loc3', '6'), ('5', 'A010', '材料部', '108室', '1');
COMMIT;

-- ----------------------------
--  Table structure for `t_emp`
-- ----------------------------
DROP TABLE IF EXISTS `t_emp`;
CREATE TABLE `t_emp` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_no` varchar(255) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `emp_sex` varchar(1) NOT NULL COMMENT '性别，0：女，1：男',
  `emp_dept_id` int(11) DEFAULT NULL,
  `t_emp_dept` varchar(255) NOT NULL DEFAULT '无',
  `emp_entry_time` varchar(255) NOT NULL,
  `emp_create_time` datetime DEFAULT NULL,
  `emp_education` varchar(255) DEFAULT NULL,
  `emp_email` varchar(255) DEFAULT NULL,
  `emp_phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_emp`
-- ----------------------------
BEGIN;
INSERT INTO `t_emp` VALUES ('1', 'E1001', '李雷', '1', '1', '总经办', '2020-03-21', '2020-03-15 21:38:09', '本科', '625372291@qq.com', '13413413413'), ('2', 'E1002', '张三', '0', '2', '渠道部', '2020-03-20', '2020-03-20 14:10:49', '本科', 'njwb@email.com', '13131313131'), ('3', 'eno3', 'ename3', '1', '2', '渠道部', '2020-03-22', '2020-03-22 10:29:29', '本科', 'e3333@163.com', '13555555555'), ('4', 'eno4', 'ename4', '1', '1', '总经办', '2020-03-15', '2020-03-22 12:01:45', '本科', 'e4444@163.com', '13456789000'), ('5', 'A001_0011', 'Q博士', '女', '1', '无', '2020-04-07 15:37:22', null, '博士', 'boshi@163.com', '12312312312');
COMMIT;

-- ----------------------------
--  Table structure for `t_holiday`
-- ----------------------------
DROP TABLE IF EXISTS `t_holiday`;
CREATE TABLE `t_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_holiday_no` varchar(11) DEFAULT NULL,
  `t_holiday_user` varchar(255) DEFAULT NULL,
  `t_holiday_type` varchar(255) DEFAULT NULL,
  `t_holiday_bz` varchar(100) DEFAULT NULL,
  `t_start_time` varchar(255) DEFAULT NULL,
  `t_end_time` varchar(255) DEFAULT NULL,
  `t_holiday_status` varchar(20) DEFAULT NULL,
  `t_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_holiday`
-- ----------------------------
BEGIN;
INSERT INTO `t_holiday` VALUES ('1', 'QJ1001', '张三丰', '事假', '11', '2020-03-19 13:45:58', '2020-03-21 13:46:00', '已提交', '2020-03-19 13:46:13'), ('6', null, 'admin', '事假', 'test', '2020-03-02', '2020-03-03', '草稿', '2020-03-22 23:04:25');
COMMIT;

-- ----------------------------
--  Table structure for `t_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `href_url` varchar(200) DEFAULT NULL,
  `menu_parent_id` int(11) DEFAULT NULL,
  `menu_create_time` date DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_menu`
-- ----------------------------
BEGIN;
INSERT INTO `t_menu` VALUES ('1', '人事管理', '', null, '2020-03-10'), ('2', '财务管理', '', null, '2020-03-10'), ('3', '系统管理', '', null, '2020-03-10'), ('4', '部门管理', 'njwb/dept/dept.html', '1', '2020-03-10'), ('5', '员工管理', '', '1', '2020-03-10'), ('6', '请假管理', '', '1', '2020-03-10'), ('7', '报销管理', '', '2', '2020-03-10'), ('8', '账户维护', '', '3', '2020-03-10'), ('9', '密码重置', '', '3', '2020-03-10'), ('10', '角色管理', '', '3', '2020-03-10'), ('11', '权限管理', '', '3', '2020-03-10'), ('12', '系统退出', '', '3', '2020-03-10');
COMMIT;

-- ----------------------------
--  Table structure for `t_money`
-- ----------------------------
DROP TABLE IF EXISTS `t_money`;
CREATE TABLE `t_money` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `t_money_no` varchar(255) DEFAULT NULL,
  `t_money_user` varchar(255) DEFAULT NULL,
  `t_money_type` varchar(255) DEFAULT NULL,
  `t_money_money` double DEFAULT NULL,
  `t_money_time` datetime DEFAULT NULL,
  `t_money_status` varchar(255) DEFAULT NULL,
  `t_money_bz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_money`
-- ----------------------------
BEGIN;
INSERT INTO `t_money` VALUES ('1', 'M1', '张三丰', '办公费', '1536.5', '2020-03-20 02:37:00', '草稿', '111111'), ('3', null, 'admin', '办公费', '123', '2020-03-23 00:55:13', '已提交', 'mbz2'), ('4', null, 'lisi', '差旅费', '123', '2020-03-23 11:48:06', '已提交', '123'), ('5', null, 'lisi', '招待费', '123', '2020-03-23 11:48:14', '已提交', '123');
COMMIT;

-- ----------------------------
--  Table structure for `t_my_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_my_user`;
CREATE TABLE `t_my_user` (
  `my_id` int(11) NOT NULL AUTO_INCREMENT,
  `my_account` varchar(255) DEFAULT NULL,
  `my_pwd` varchar(255) DEFAULT NULL,
  `my_emp_no` int(11) DEFAULT NULL,
  `my_role_id` int(11) DEFAULT NULL,
  `my_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`my_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_my_user`
-- ----------------------------
BEGIN;
INSERT INTO `t_my_user` VALUES ('1', '123', '123', '1', '1', '2020-04-07 16:22:13');
COMMIT;

-- ----------------------------
--  Table structure for `t_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `t_permissions`;
CREATE TABLE `t_permissions` (
  `per_id` int(11) NOT NULL,
  `per_role_id` int(11) NOT NULL,
  `per_menu_id` int(11) NOT NULL,
  `per_create_time` date DEFAULT NULL,
  PRIMARY KEY (`per_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `t_permissions` VALUES ('1', '1', '1', '2020-03-10'), ('2', '1', '2', '2020-03-10'), ('3', '1', '3', '2020-03-10'), ('4', '1', '8', '2020-03-10'), ('5', '1', '9', '2020-03-10'), ('6', '1', '10', '2020-03-10'), ('7', '2', '1', '2020-03-10'), ('8', '2', '2', '2020-03-10'), ('9', '2', '3', '2020-03-10'), ('10', '2', '6', '2020-03-10'), ('11', '2', '7', '2020-03-10'), ('12', '2', '12', '2020-03-10');
COMMIT;

-- ----------------------------
--  Table structure for `t_reader`
-- ----------------------------
DROP TABLE IF EXISTS `t_reader`;
CREATE TABLE `t_reader` (
  `re_id` int(11) NOT NULL AUTO_INCREMENT,
  `re_name` varchar(20) NOT NULL,
  `re_phone` char(11) NOT NULL,
  `re_card_id` int(11) NOT NULL,
  PRIMARY KEY (`re_id`),
  KEY `fk_card_id` (`re_card_id`),
  CONSTRAINT `fk_card_id` FOREIGN KEY (`re_card_id`) REFERENCES `t_card` (`card_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_reader`
-- ----------------------------
BEGIN;
INSERT INTO `t_reader` VALUES ('1', '张三', '12345678900', '1'), ('2', '李四', '12345555555', '2');
COMMIT;

-- ----------------------------
--  Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_role_name` varchar(30) NOT NULL,
  `t_create_time` date DEFAULT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_role`
-- ----------------------------
BEGIN;
INSERT INTO `t_role` VALUES ('1', '管理员', '2020-03-10', '管理员', '2020-04-03'), ('2', '普通用户', null, '普通用户', '2020-04-04'), ('3', '人事专员', '2020-03-10', '人事专员', '2020-04-03');
COMMIT;

-- ----------------------------
--  Table structure for `t_score`
-- ----------------------------
DROP TABLE IF EXISTS `t_score`;
CREATE TABLE `t_score` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `score_stu_id` int(11) NOT NULL,
  `score_sub_id` int(11) NOT NULL,
  `score_cent` int(11) NOT NULL,
  PRIMARY KEY (`score_id`),
  KEY `fk_stu2_id` (`score_stu_id`),
  KEY `fk_sub_id` (`score_sub_id`),
  CONSTRAINT `fk_stu2_id` FOREIGN KEY (`score_stu_id`) REFERENCES `t_student` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sub_id` FOREIGN KEY (`score_sub_id`) REFERENCES `t_subject` (`sub_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_score`
-- ----------------------------
BEGIN;
INSERT INTO `t_score` VALUES ('1', '1', '1', '80'), ('2', '1', '2', '90'), ('3', '2', '1', '70'), ('4', '2', '2', '60');
COMMIT;

-- ----------------------------
--  Table structure for `t_student`
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_name` varchar(20) NOT NULL,
  `st_sex` char(2) NOT NULL,
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_student`
-- ----------------------------
BEGIN;
INSERT INTO `t_student` VALUES ('1', '张三', '男'), ('2', '李四', '女');
COMMIT;

-- ----------------------------
--  Table structure for `t_subject`
-- ----------------------------
DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE `t_subject` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(20) NOT NULL,
  `sub_cent` int(11) NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_subject`
-- ----------------------------
BEGIN;
INSERT INTO `t_subject` VALUES ('1', 'subname1', '4'), ('2', 'subname2', '5');
COMMIT;

-- ----------------------------
--  Table structure for `t_sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_config_type` varchar(20) DEFAULT NULL,
  `t_config_key` varchar(255) DEFAULT NULL,
  `t_config_page_value` varchar(255) DEFAULT NULL,
  `t_create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_sys_config`
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_config` VALUES ('1', '账号', '正常', '0', '2020-03-15 21:41:32'), ('2', '账号', '注销', '1', '2020-03-20 11:15:00'), ('3', '请假', '事假', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_user_account` varchar(20) NOT NULL,
  `t_user_pwd` varchar(20) NOT NULL,
  `t_emp_no` varchar(5) NOT NULL,
  `t_role_id` varchar(11) DEFAULT NULL,
  `t_create_time` date DEFAULT NULL,
  `t_user_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_user`
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES ('1', 'admin', 'admin', 'E1001', '管理员', '2020-03-10', '正常'), ('2', 'zhangsan', 'zs', 'E1002', '普通用户', '2020-03-10', '正常'), ('3', 'lisi', 'lisi', 'e3', '普通用户', '2020-03-23', '正常'), ('4', 'wangwu', 'wangwu', 'e4', '人事专员', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
