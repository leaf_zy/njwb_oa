<%@ page contentType="image/jpeg"
	import="java.util.*,java.awt.*,java.io.*,java.awt.image.*,javax.imageio.*"
	pageEncoding="utf-8"%>

<%!// jsp验证码
	Color getRandColor(int fc, int bc) {//给定范围获得随机颜色
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}%>
<%
	//设置页面不缓存
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);

	// 在内存中创建图象
	int width = 80, height = 20;//设置图片的宽度与高度
	BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	String str="abcdefghijklmnopqrstuvwxyz0123456789";
	// 获取图形上下文
	Graphics g = image.getGraphics();

	//生成随机类
	Random random = new Random();

	// 设定背景色
	g.setColor(getRandColor(200, 250));//设置笔刷的颜色
	g.fillRect(0, 0, width, height);//绘制带填充色的矩形，，0, 0起始的坐标，宽度与高度
	//g.drawRect(x, y, width, height);//绘制空心的矩形

	//设定字体
	g.setFont(new Font("Times New Roman", Font.PLAIN, 18));

	//画边框
	//g.setColor(new Color());
	//g.drawRect(0,0,width-1,height-1);

	// 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
	g.setColor(getRandColor(160, 200));
	for (int i = 0; i < 155; i++) {
		int x = random.nextInt(width);
		int y = random.nextInt(height);
		int xl = random.nextInt(12);
		int yl = random.nextInt(12);
		g.drawLine(x, y, x + xl, y + yl);//划线（两点的坐标，（x, y），（x + xl, y + yl））
	}

	// 取随机产生的认证码(4位数字)
	String sRand = "";
	/*
	for (int i = 0; i < 4; i++) {//写四次，每次只写一个字母
		//String rand = String.valueOf(random.nextInt(10));//每次产生一个0-9数字
		 //数字与字母
		 int begin = random.nextInt(str.length()-1);
		 String rand= str.substring(begin,begin+1).toUpperCase();
		sRand += rand;
		// 将认证码显示到图象中
		g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));// 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
		g.drawString(rand, 13 * i + 6, 16);//rand--要写入的内容，（13 * i + 6, 16）从某个坐标开始写起，
	}
	*/
	//人类验证，数学运算
	int num1 = random.nextInt(10);
	sRand+=num1+" ";
	int num2 = random.nextInt(10);
	//操作符+，-，*   
	int op = random.nextInt(20)%3;//（0，1，2）
	int result = 0;
	if(op==0){
		sRand+="+ ";
		result = num1+num2;
	}else if(op==1){
		sRand+="- ";
		result = num1-num2;
	}else if(op==2){
		sRand+="* ";
		result = num1*num2;
	}
	sRand +=num2+" = ?" ;
	g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
	g.drawString(sRand, 5, 16);
	// 将认证码存入SESSION中
	//session.setAttribute("rand", sRand);
	session.setAttribute("result", result);

	// 图象生效
	g.dispose();
	OutputStream output = response.getOutputStream();

	// 输出图象到页面
	ImageIO.write(image, "JPEG", response.getOutputStream());
	output.flush();
	out.clear();
	out = pageContext.pushBody();
%>
