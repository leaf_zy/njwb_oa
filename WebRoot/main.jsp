<%@page import="com.wbhz.entity.User"%>
<%@page import="com.wbhz.service.*"%>
<%@page import="com.wbhz.util.*"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>main.jsp</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

 	<style type="text/css">
  	.hide{
  		display: none;
  	
  	}
    </style>
   	<script type="text/javascript">
   	$(function(){
   		  //找到所有的li,且class=menu
   		  //alert($("li[class='menu']").length);
   		  
   		  
   		  //存在的问题：一级菜单和二级菜单能正常的显示与隐藏，但是当点击二级菜单，发现二级菜单也跟着隐藏
   		  
   		  //吸取一个经验：在网页元素排版时，要兼顾后期的js操作
   		  //一个合理的网页布局，会让js获取元素时非常遍历，否则就很痛苦
   		  /**
   		  $("li[class='menu']").each(function(){
   		  	  $(this).click(function(){
   		  	      $(this).children(".hide").slideToggle();
   		  	  });
   		  
   		  });
   		  */
   		  
   		  //对所有的span标签设置单击事件
   		  
   		  //alert($("span").length);  4个
   		  
   		  //alert($("li[class='menu'] span").length);
   		  
   		  $("li[class='menu'] span").each(function(){
   		  		$(this).click(function(){
   		  			  //this代表的是span
   		  			  $(this).siblings(".hide").slideToggle();
   		  		
   		  		});
   		  
   		  });
   	
   	});
   
   	</script> 
    <script type="text/javascript">
		function loginOut(){
			if(confirm("是否退出本系统?")){
				location.href="login.jsp";
			}
		}
	</script>
   
  </head>
  
  <body>
  	<!-- 判断用户是否登录 -->
 	<%-- <%@include file="isLogin.jsp" %> --%>
  	<div id = "mainDiv">
	  	<div id = "header">
	    	<div id = "logoDiv" class="lft">
	    		南京网博教育集团
	    	</div>
	    	<div id = "userDiv" class="rft">
	    		<%=request.getParameter("userName") %>
	    	</div>
	    </div>
	    <div id = "welcomeDiv">
	    	欢迎使用网博管理系统
	    </div>
	    
	    
	    <div id = "contentDiv">
	    	<div id = "content-left" class="lft">
	    		<ul>
	    		
	    			<li class="menu">
	    				<span>人事管理</span>
	    				<ul class="hide">
	    					<li class="menu-sub"><a href="dept/getSomeMood.do"  id="deptManager"   target="contentPage">部门管理</a></li>
	    					<li class="menu-sub"><a href="emp/getSomeMood.do"  id="deptManager"   target="contentPage">员工管理</a></li>
	    					<li class="menu-sub"><a href="holiday/getSomeMood.do"  id="deptManager"   target="contentPage">请假管理</a></li>
	    				</ul>
	    			</li>
	    			
	    			<li class="menu">
	    				<span>财务管理</span>
	    				<ul  class="hide">
	    					<li class="menu-sub"><a href="money/getSomeMood.do"  id="deptManager"   target="contentPage">报销管理</a></li>
	    				</ul>
	    			
	    			</li>	    			
	    		    <li class="menu">
	    				<span>系统管理</span>
	    				<ul class="hide">
	    					<li class="menu-sub"><a href="user/getSomeMood.do"  id="deptManager"   target="contentPage">账户管理</a></li>
	    					<li class="menu-sub"><a href="role/getSomeMood.do"  id="deptManager"   target="contentPage">角色管理</a></li>
	    					<li class="menu-sub"><a href="permissions/getSomeMood.do"  id="deptManager"   target="contentPage">权限管理</a></li>
	    					<li class="menu-sub"><a href="user/userPwd.jsp"  id="deptManager"   target="contentPage">密码重置</a></li>
	    					<li class="menu-sub"><a href="javascript:loginOut()">系统退出</a></li>
	    					
	    				</ul>
	    			
	    			</li>

	    		</ul>
	    		
	    		
	    	</div>
	    	
	    	<div id = "content-right" class="rft">
	    		<iframe src="" name="contentPage" scrolling="yes" frameborder="0" width="788px" height="470px">
	    		</iframe>
	    	</div>
	    </div>
	    
	    <div id = "footer">
	    	<span>&copy;版权归属南京网博江北总部</span>
	    </div>
  	
  	</div>
   
  </body>
</html>
