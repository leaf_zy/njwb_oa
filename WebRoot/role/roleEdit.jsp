<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>角色编辑</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
	<%
		String id=request.getParameter("id");
		String rname=request.getParameter("rname");
	 %>
  </head>
  
	<body>
	<form action="role/update.do" method="post">

		<table id="deptEditTable">
			<tr>
				<td>
					角色ID:
				</td>
				<td>
					<input type="text" name="id" id="" value="<%=id %>" readonly="readonly" style="background-color: #F0F0F0;" />
				</td>
			</tr>
			<tr>
				<td>
					角色名称:
				</td>
				<td>
					<input type="text" name="name" id="" value="<%=rname %>"/>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<input type="submit" value="修改" />
					<input type="reset" value="重置" />
					<a href="role/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>


	</form>
	</body>
</html>
