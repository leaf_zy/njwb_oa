<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'deptEdit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="money/update.do?from=moneyEdit" method="post">

		<table id="deptEditTable">
			<c:forEach var="mList" items="${mList }">
			<tr>
				<td>
					报销编号:
				</td>
				<td>
					<input type="text" name="id" id="id" value="${mList.id }" readonly="readonly" style="background-color: #F0F0F0;" />
				</td>
			</tr>
			<tr>
				<td>
					申请人姓名:
				</td>
				<td>
					<input type="text" name="muser" id="" value="${mList.muser }" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td>
					报销金额:
				</td>
				<td>
					<input type="text" name="money" id="deptName" value="${mList.money }" />
				</td>
			</tr>
			<tr>
				<td>
					申请类型:
				</td>
				<td>
					<select name="type">
						<option value="${mList.mtype }" selected="selected">${mList.mtype }</option>
						<option value="差旅费">差旅费</option>
						<option value="招待费">招待费</option>
						<option value="办公费">办公费</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					报销摘要:
				</td>
				<td>
					<input type="text" name="mbz" id="" value="${mList.mbz }"/>
				</td>
			</tr>
			<tr>
				<td>
					申请状态:
				</td>
				<td>
					<select name="status">
						<option value="${mList.mstatus }" selected="selected">${mList.mstatus }</option>
						<c:if test="${mList.mstatus == '已提交' }">
							<option value="草稿">草稿</option>
						</c:if>
						<c:if test="${mList.mstatus == '草稿' }">
							<option value="已提交">已提交</option>
						</c:if>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="修改" />
					<input type="reset" value="重置" />
					<a href="money/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
			</c:forEach>
		</table>


	</form>
	</body>
</html>
