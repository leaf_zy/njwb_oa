<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="js/data.js"></script>
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="money/insert.do?from=moneyAdd" method="post" id="form1">
		<input type="hidden" name="name" value="${userName }" />
		<table id="deptEditTable">
			<tr>
				<td>
					报销类型:
				</td>
				<td>
					<select name="mtype">
						<option value="1">--请选择--</option>
						<option value="差旅费">差旅费</option>
						<option value="招待费">招待费</option>
						<option value="办公费">办公费</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					报销摘要:
				</td>
				<td>
					<input type="text" name="mbz" id="deptName" />
				</td>
			</tr>
			<tr>
				<td>
					报销金额:
				</td>
				<td>
					<input type="text" name="moneyNum" id="deptName" />
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<input type="submit" value="提交" name="sub" />
					<input type = "submit" value="草稿" name="sub"/>
					<input type="reset" value="重置" />
					<a href="money/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>

	</form>
	</body>

</html>
