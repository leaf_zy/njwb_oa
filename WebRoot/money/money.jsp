<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>报销管理</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/layer/layer.js"></script>
	<script type="text/javascript">
		
		function del(){
			var result = window.confirm("确认要删除吗?");
			if(true == result){
				alert("删除成功");
			}
		}
	
	</script>
  </head>
  <body>
         	<h1 class="title">首页  &gt;&gt;报销管理 </h1>
         		
         	<div class="add">
				<form action="money/search.do" method="post">
					<table>
						<tr>
							<td>报销类型：</td>
							<td>
								<select name="type">
									<option value="1">--请选择--</option>
									<option value="差旅费">差旅费</option>
									<option value="招待费">招待费</option>
									<option value="办公费">办公费</option>
								</select>
							</td>
							<td>申请状态：</td>
							<td>
								<select name="status">
									<option value="1">--请选择--</option>
									<option value="草稿">草稿</option>
									<option value="已提交">已提交</option>
								</select>
							</td>
							<td>&nbsp;<input type="submit" value="查询" /></td>
						</tr>
					</table>
				</form>
         		<a href="money/insert.do?from=money" target="contentPage"><img alt="" src="img/add.png" width="18px" height="18px">添加报销</a>
         	</div>
         	<br />
         	
         	<table class="deptInfo">
         		<thead>
	         		<tr class="titleRow">
						<td>报销编号</td>
						<td>申请人</td>
						<td>报销类型</td>
						<td>金额</td>
						<td>申请时间</td>
						<td>申请状态</td>
	         			<td>操作列表</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="mood" items="${some}">
						<tr>
							<td>${mood.id }</td>
							<td>${mood.muser }</td>
							<td>${mood.mtype }</td>
							<td>${mood.money }</td>
							<td>${mood.ctime }</td>
							<td>${mood.mstatus }</td>
							<td>
								<a href="money/delete.do?id=${mood.id }&status=${mood.mstatus }"><img alt="" src="img/delete.png" class="operateImg" onclick="del()"></a>
								<a href="money/update.do?from=money&id=${mood.id}&type=${mood.mtype }&money=${mood.money }&status=${mood.mstatus }&mbz=${mood.mbz }" target="contentPage"><img alt="" src="img/edit.png" class="operateImg"></a>
								<a href="money/moneyDetail.jsp?id=${mood.id}&user=${mood.muser }&type=${mood.mtype }&money=${mood.money }&time=${mood.ctime}&status=${mood.mstatus }&mbz=${mood.mbz }" target="contentPage"><img alt="" src="img/detail.png" class="operateImg"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot></tfoot>
         	</table>
         	<center>
			<a href="money/getSomeMood.do?currentPage=1">首页</a> &nbsp;
			<c:if test="${currentPage==1}" var="pre">
				<a href="money/getSomeMood.do?currentPage=1">上一页</a> &nbsp;
			</c:if>
			<c:if test="${ ! pre }">
				<a href="money/getSomeMood.do?currentPage=${currentPage-1 }">上一页</a> &nbsp;
			</c:if>
			<c:if test="${currentPage==totalCount}" var="next">
				<a href="money/getSomeMood.do?currentPage=${totalCount }">下一页</a>&nbsp;
			</c:if>
			<c:if test="${ ! next }">
				<a href="money/getSomeMood.do?currentPage=${currentPage+1 }">下一页</a>&nbsp;
			</c:if>
			<a href="money/getSomeMood.do?currentPage=${totalCount }">末 页</a><br />
			一共${totalCount }页,一共${total }条
			</center>
  </body>  
</html>
