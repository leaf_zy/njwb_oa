<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>permissions.jsp</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/layer/layer.js"></script>
	<script type="text/javascript">
		
		function del(){
			var result = window.confirm("确认要删除吗?");
			if(true == result){
				alert("删除成功");
			}
		}
	
	</script>
  </head>
  <body>
         	<h1 class="title">首页  &gt;&gt;权限管理 </h1>
         	
         	<div class="add">
				<form action="permissions/search.do" method="post">
					<table>
						<tr>
							<td>角色：</td>
							<td>
								<%-- <select name="role">
										<option value="1">--请选择--</option>
									<c:forEach var="ls" items="${rList }">
	    								<option value="${ls.rname }">${ls.rname }</option>
	         						</c:forEach>
								</select> --%>
								<select name="role">
									<option value="1">--请选择--</option>
									<option value="管理员">管理员</option>
									<option value="普通用户">普通用户</option>
									<option value="人事专员">人事专员</option>
								</select>
							</td>
							<td>菜单：</td>
							<td>
								<select name="menu">
									<option value="1">--请选择--</option>
									<option value="草稿">草稿</option>
									<option value="已提交">已提交</option>
								</select>
							</td>
							<td>&nbsp;<input type="submit" value="查询" /></td>
						</tr>
					</table>
				</form>
         		<a href="permissions/insert.do?from=permissions" target="contentPage"><img alt="" src="img/add.png" width="18px" height="18px">添加权限</a>
         	</div>
         	<br />
         	
         	<table class="deptInfo">
         		<thead>
	         		<tr class="titleRow">
						<td>角色ID</td>
						<td>角色名称</td>
						<td>菜单ID</td>
						<td>菜单名称</td>
	         			<td>操作列表</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="mood" items="${some}">
						<tr>
							<td>${mood.rid }</td>
							<td>${mood.rname }</td>
							<td>${mood.mid }</td>
							<td>${mood.mname }</td>
							<td>
								<a href="permissions/delete.do?id=${mood.id }"><img alt="" src="img/delete.png" class="operateImg" onclick="del()"></a>
								<a href="permissions/deptEdit.jsp?id=${mood.id }&rid=${mood.rid }&mid=${mood.mid }" target="contentPage"><img alt="" src="img/edit.png" class="operateImg"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot></tfoot>
         	</table>
         	<center>
			<a href="permissions/getSomeMood.do?currentPage=1">首页</a> &nbsp;
			<c:if test="${currentPage==1}" var="pre">
				<a href="permissions/getSomeMood.do?currentPage=1">上一页</a> &nbsp;
			</c:if>
			<c:if test="${ ! pre }">
				<a href="permissions/getSomeMood.do?currentPage=${currentPage-1 }">上一页</a> &nbsp;
			</c:if>
			<c:if test="${currentPage==totalCount}" var="next">
				<a href="permissions/getSomeMood.do?currentPage=${totalCount }">下一页</a>&nbsp;
			</c:if>
			<c:if test="${ ! next }">
				<a href="permissions/getSomeMood.do?currentPage=${currentPage+1 }">下一页</a>&nbsp;
			</c:if>
			<a href="permissions/getSomeMood.do?currentPage=${totalCount }">末 页</a><br />
			一共${totalCount }页,一共${total }条
			</center>
  </body>  
</html>
