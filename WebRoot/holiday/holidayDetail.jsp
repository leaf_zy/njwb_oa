<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>部门明细</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String type=request.getParameter("type");
		String reason=request.getParameter("reason");
		String sTime=request.getParameter("sTime");
		String eTime=request.getParameter("eTime");
		String status=request.getParameter("status");
		String createTime = request.getParameter("createTime");
	 %>
  </head>
  
	<body>
	<table>
		<tr>
			<td>
				请假编号:
			</td>
			<td>
				<%=id %>
			</td>
		</tr>
		<tr>
			<td>
				申请人姓名:
			</td>
			<td>
				<%=name %>
			</td>
		</tr>
		<tr>
			<td>
				请假类型:
			</td>
			<td>
				<%=type %>
			</td>
		</tr>
		<tr>
			<td>
				请假事由:
			</td>
			<td>
				<%=reason %>
			</td>
		</tr>
		<tr>
			<td>
				开始时间:
			</td>
			<td>
				<%=sTime %>
			</td>
		</tr>
		<tr>
			<td>
				结束时间:
			</td>
			<td>
				<%=eTime %>
			</td>
		</tr>
		<tr>
			<td>
				请假状态:
			</td>
			<td>
				<%=status %>
			</td>
		</tr>
		<tr>
			<td>
				创建时间:
			</td>
			<td>
				<%=createTime %>
			</td>
		</tr>
		<tr>
	</table>
	<a href="holiday/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
	</body>
</html>
