<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'deptEdit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="js/data.js"></script>
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="holiday/update.do?from=holidayEdit" method="post">

		<table id="deptEditTable">
			<c:forEach var="holList" items="${holList }">
			<tr>
				<td>
					请假编号:
				</td>
				<td>
					<input type="text" name="id" id="id" value="${holList.id }" readonly="readonly" style="background-color: #F0F0F0;" />
				</td>
			</tr>
			<tr>
				<td>
					申请人姓名:
				</td>
				<td>
					<input type="text" name="name" id="name" value="${holList.huser }"/>
				</td>
			</tr>
			<tr>
				<td>
					申请类型:
				</td>
				<td>
					<select name="type">
						<!-- 事假、婚假、年假、调休、病假、丧假 -->
						<option value="${holList.htype }" selected="selected">${holList.htype }</option>
						<option value="事假">事假</option>
						<option value="婚假">婚假</option>
						<option value="年假">年假</option>
						<option value="调休">调休</option>
						<option value="病假">病假</option>
						<option value="丧假">丧假</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					请假事由:
				</td>
				<td>
					<input type="text" name="reason" id="" value="${holList.hbz }" />
				</td>
			</tr>
			<tr>
				<td>
					开始时间:
				</td>
				<td>
					<input type="text" value="${holList.stime }" name="sTime" onclick='popUpCalendar(this, this, "yyyy-mm-dd")' size="15"
						maxlength="15" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td>
					结束时间:
				</td>
				<td>
					<input type="text" value="${holList.etime }" name="eTime" onclick='popUpCalendar(this, this, "yyyy-mm-dd")' size="15"
						maxlength="15" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td>
					申请状态:
				</td>
				<td>
					<select name="status">
						<option value="${holList.hstatus }" selected="selected">${holList.hstatus }</option>
						<c:if test="${holList.hstatus == '已提交' }">
							<option value="草稿">草稿</option>
						</c:if>
						<c:if test="${holList.hstatus == '草稿' }">
							<option value="已提交">已提交</option>
						</c:if>
					</select>
				</td>
			</tr>
			</c:forEach>
			<tr>
				<td colspan="2">
					<input type="submit" value="修改" />
					<input type="reset" value="重置" />
					<a href="holiday/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>


	</form>
	</body>
</html>
