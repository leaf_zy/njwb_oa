<form action="emp/search.do" method="post"></form><%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>holiday.jsp</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/layer/layer.js"></script>
	<script type="text/javascript">
		
		function del(){
			var result = window.confirm("确认要删除吗?");
			if(true == result){
				alert("删除成功");
			}
		}
	
	</script>
  </head>
  <body>
         	<h1 class="title">首页  &gt;&gt;请假管理 </h1>
         		
         	<div class="add">
				<form action="holiday/search.do" method="post">
					<table>
						<tr>
							<td>申请人：</td>
							<td><input type="text" name="hname" /></td>
							<td>请假类型：</td>
							<td>
								<select name="htype">
									<!-- 事假、婚假、年假、调休、病假、丧假 -->
									<option value="1">--请选择--</option>
									<option value="事假">事假</option>
									<option value="婚假">婚假</option>
									<option value="年假">年假</option>
									<option value="调休">调休</option>
									<option value="病假">病假</option>
									<option value="丧假">丧假</option>
								</select>
							</td>
							<td>申请状态：</td>
							<td>
								<select name="hstatus">
									<option value="1">--请选择--</option>
									<option value="草稿">草稿</option>
									<option value="已提交">已提交</option>
								</select>
							</td>
							<td>&nbsp;<input type="submit" value="查询" /></td>
						</tr>
					</table>
				</form>
         		<a href="holiday/insert.do?from=holiday" target="contentPage"><img alt="" src="img/add.png" width="18px" height="18px">添加请假</a>
         	</div>
         	<br />
         	
         	<table class="deptInfo">
         		<thead>
	         		<tr class="titleRow">
	         			<td>请假编号</td>
	         			<td>申请人</td>
	         			<td>申请类型</td>
	         			<td>请假事由</td>
	         			<td>开始时间</td>
	         			<td>结束时间</td>
	         			<td>申请状态</td>
	         			<td>提交时间</td>
	         			<td>操作列表</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="mood" items="${some}">
						<tr>
							<td>${mood.id }</td>
							<td>${mood.huser }</td>
							<td>${mood.htype }</td>
							<td>${mood.hbz }</td>
							<td>${mood.stime }</td>
							<td>${mood.etime }</td>
							<td>${mood.hstatus }</td>
							<td>${mood.ctime }</td>
							<td>
								<a href="holiday/delete.do?id=${mood.id}&status=${mood.hstatus}"><img alt="" src="img/delete.png" class="operateImg" onclick="del()"></a>
								<a href="holiday/update.do?from=holiday&id=${mood.id}&name=${mood.huser}&type=${mood.htype}&reason=${mood.hbz}&sTime=${mood.stime}&eTime=${mood.etime}&status=${mood.hstatus}" target="contentPage"><img alt="" src="img/edit.png" class="operateImg"></a>
								<a href="holiday/holidayDetail.jsp?id=${mood.id}&name=${mood.huser}&type=${mood.htype}&reason=${mood.hbz}&sTime=${mood.stime}&eTime=${mood.etime}&status=${mood.hstatus}&createTime=${mood.ctime }" target="contentPage"><img alt="" src="img/detail.png" class="operateImg"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot></tfoot>
         	</table>
         	<center>
			<a href="holiday/getSomeMood.do?currentPage=1">首页</a> &nbsp;
			<c:if test="${currentPage==1}" var="pre">
				<a href="holiday/getSomeMood.do?currentPage=1">上一页</a> &nbsp;
			</c:if>
			<c:if test="${ ! pre }">
				<a href="holiday/getSomeMood.do?currentPage=${currentPage-1 }">上一页</a> &nbsp;
			</c:if>
			<c:if test="${currentPage==totalCount}" var="next">
				<a href="holiday/getSomeMood.do?currentPage=${totalCount }">下一页</a>&nbsp;
			</c:if>
			<c:if test="${ ! next }">
				<a href="holiday/getSomeMood.do?currentPage=${currentPage+1 }">下一页</a>&nbsp;
			</c:if>
			<a href="holiday/getSomeMood.do?currentPage=${totalCount }">末 页</a><br />
			一共${totalCount }页,一共${total }条
			</center>
  </body>  
</html>
