<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="js/data.js"></script>
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="holiday/insert.do?from=holidayAdd" method="post" id="form1">
		<input type="hidden" name="name" value="${userName }" />
		<table id="deptEditTable">
			<tr>
				<td>
					请假类型:
				</td>
				<td>
					<select name="type">
						<!-- 事假、婚假、年假、调休、病假、丧假 -->
						<option value="1">--请选择--</option>
						<option value="事假">事假</option>
						<option value="婚假">婚假</option>
						<option value="年假">年假</option>
						<option value="调休">调休</option>
						<option value="病假">病假</option>
						<option value="丧假">丧假</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					请假事由:
				</td>
				<td>
					<input type="text" name="hbz" id="deptName" />
				</td>
			</tr>
			<tr>
				<td>
					开始时间:
				</td>
				<td>
					<input type="text" name="stime" onclick='popUpCalendar(this, this, "yyyy-mm-dd")' size="15" maxlength="15"
						readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td>
					结束时间:
				</td>
				<td>
					<input type="text" name="etime" onclick='popUpCalendar(this, this, "yyyy-mm-dd")' size="15" maxlength="15"
						readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="提交" name="sub" />
					<input type="submit" value="草稿" name="sub" />
					<input type="reset" value="重置" />
					<a href="holiday/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>
	</form>
	</body>

</html>
