<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>部门明细</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%
		String no=request.getParameter("no");
		String name=request.getParameter("name");
		String loc=request.getParameter("loc");
		String manager=request.getParameter("manager");
	 %>
  </head>
  
	<body>
	<table>
		<tr>
			<td>
				部门编号:
			</td>
			<td>
				<%=no %>
			</td>
		</tr>
		<tr>
			<td>
				部门名称:
			</td>
			<td>
				<%=name %>
			</td>
		</tr>
		
		<tr>
			<td>
				部门位置:
			</td>
			<td>
				<%=loc %>
			</td>
		</tr>
		
		<tr>
			<td>
				部门负责人:
			</td>
			<td>
				<%=manager %>
			</td>
		</tr>
	</table>
	<a href="dept/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
	</body>
</html>
