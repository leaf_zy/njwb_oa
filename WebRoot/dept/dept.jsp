<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>dept.jsp</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/layer/layer.js"></script>
	<script type="text/javascript">
		
		function del(){
			var result = window.confirm("确认要删除吗?");
			if(true == result){
				alert("删除成功");
			}
		}
	
	</script>
  </head>
  <body>
         	<h1 class="title">首页  &gt;&gt;部门管理 </h1>
         	
         	<div class="add">
         		<a href="dept/deptAdd.jsp" target="contentPage"><img alt="" src="img/add.png" width="18px" height="18px">添加部门</a>
         	</div>
         	
         	<table class="deptInfo">
         		<thead>
	         		<tr class="titleRow">
	         			<td>部门编号</td>
	         			<td>部门名称</td>
	         			<td>部门位置</td>
	         			<td>部门负责人</td>
	         			<td>操作列表</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="mood" items="${some}">
						<tr>
							<td>${mood.dno }</td>
							<td>${mood.dname }</td>
							<td>${mood.dloc }</td>
							<td>${mood.dmanager }</td>
							<td>
								<a href="dept/delete.do?no=${mood.dno}&dname=${mood.dname }"><img alt="" src="img/delete.png" class="operateImg" onclick="del()"></a>
								<a href="dept/deptEdit.jsp?no=${mood.dno}&name=${mood.dname }&loc=${mood.dloc }&manager=${mood.dmanager }" target="contentPage"><img alt="" src="img/edit.png" class="operateImg"></a>
								<a href="dept/deptDetail.jsp?no=${mood.dno}&name=${mood.dname }&loc=${mood.dloc }&manager=${mood.dmanager }" target="contentPage"><img alt="" src="img/detail.png" class="operateImg"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot></tfoot>
         	</table>
         	<center>
			<a href="dept/getSomeMood.do?currentPage=1">首页</a> &nbsp;
			<c:if test="${currentPage==1}" var="pre">
				<a href="dept/getSomeMood.do?currentPage=1">上一页</a> &nbsp;
			</c:if>
			<c:if test="${ ! pre }">
				<a href="dept/getSomeMood.do?currentPage=${currentPage-1 }">上一页</a> &nbsp;
			</c:if>
			<c:if test="${currentPage==totalCount}" var="next">
				<a href="dept/getSomeMood.do?currentPage=${totalCount }">下一页</a>&nbsp;
			</c:if>
			<c:if test="${ ! next }">
				<a href="dept/getSomeMood.do?currentPage=${currentPage+1 }">下一页</a>&nbsp;
			</c:if>
			<a href="dept/getSomeMood.do?currentPage=${totalCount }">末 页</a><br />
			一共${totalCount }页,一共${total }条
			</center>
  </body>  
</html>
