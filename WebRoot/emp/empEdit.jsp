<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'deptEdit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="js/data.js"></script>
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="emp/update.do?from=empEdit" method="post">

		<table id="deptEditTable">
			<c:forEach var="emp" items="${emp }">
			<tr>
				<td>
					员工编号:
				</td>
				<td>
					<input type="text" name="no" id="no" value="${emp.eno }" readonly="readonly" style="background-color: #F0F0F0;" />
				</td>
			</tr>
			<tr>
				<td>
					员工名称:
				</td>
				<td>
					<input type="text" name="name" id="name" value="${emp.ena }"/>
				</td>
			</tr>
			<tr>
				<td>
					性别:
				</td>
				<td>
					<select name="sex">
						<option value="${emp.getSex2()}" selected="selected">${emp.sex }</option>
						<c:if test="${emp.sex == '男' }">
							<option value="0">女</option>
						</c:if>
						<c:if test="${emp.sex == '女' }">
							<option value="1">男</option>
						</c:if>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					所属部门:
				</td>
				<td>
					<select name="dept">
							<option value="${emp.edept }">${emp.edept }</option>
						<c:forEach var="ls" items="${deptList }">
    						<option value="${ls.dname }">${ls.dname }</option>
         				</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					入职时间:
				</td>
				<td>
					<input type="text" name="entryTime" id="entryTime" value="${emp.etime }" onclick='popUpCalendar(this, this, "yyyy-mm-dd")' size="15" maxlength="15" readonly="readonly"/>
				</td>
			</tr>
			</c:forEach>
			<tr>
				<td colspan="2">
					<input type="submit" value="修改" />
					<input type="reset" value="重置" />
					<a href="emp/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>

	</form>
	</body>
</html>
