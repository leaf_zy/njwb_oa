<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>部门明细</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
	<body>
	<table>
		<c:forEach var="emp" items="${emp }">
		<tr>
			<td>
				员工编号:
			</td>
			<td>
				${emp.eno }
			</td>
		</tr>
		<tr>
			<td>
				员工名称:
			</td>
			<td>
				${emp.ena }
			</td>
		</tr>
		<tr>
			<td>
				性别:
			</td>
			<td>
				${emp.sex }
			</td>
		</tr>
		<tr>
			<td>
				所属部门:
			</td>
			<td>
				${emp.edept }
			</td>
		</tr>
		<tr>
			<td>
				入职时间:
			</td>
			<td>
				${emp.etime }
			</td>
		</tr>
		</c:forEach>
	</table>
	<a href="emp/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
	</body>
</html>
