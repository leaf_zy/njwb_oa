<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>emp.jsp</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/layer/layer.js"></script>
	<script type="text/javascript">
		
		function del(){
			var result = window.confirm("确认要删除吗?");
			if(true == result){
				alert("删除成功");
			}
		}
	
	</script>
  </head>
  <body>
         	<h1 class="title">首页  &gt;&gt;员工管理 </h1>
         	<div class="add">
				<form action="emp/search.do" method="post">
					<table>
						<tr>
							<td>姓名：</td>
							<td><input type="text" name="ename" /></td>
							<td>部门：</td>
							<td>
								<select name="edept">
									<option value="1">--请选择--</option>
								<c:forEach var="ls" items="${deptList }">
									<option value="${ls.dname }">${ls.dname }</option>
								</c:forEach>
								</select>
							</td>
							<td>&nbsp;<input type="submit" value="查询" /></td>
						</tr>
					</table>
				</form>
         		<a href="emp/insert.do?from=emp" target="contentPage"><img alt="" src="img/add.png" width="18px" height="18px">添加员工</a>
         	</div>
         	<br />
         	
         	<table class="deptInfo">
         		<thead>
	         		<tr class="titleRow">
	         			<td>员工编号</td>
	         			<td>员工名称</td>
	         			<td>性别</td>
	         			<td>所属部门</td>
	         			<td>入职时间</td>
	         			<td>操作列表</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="mood" items="${some }">
						<tr>
							<td>${mood.eno }</td>
							<td>${mood.ena }</td>
							<td>${mood.sex }</td>
							<td>${mood.edept }</td>
							<td>${mood.etime }</td>
							<td>
								<a href="emp/delete.do?no=${mood.eno }"><img alt="" src="img/delete.png" class="operateImg" onclick="del()"></a>
								<a href="emp/update.do?from=emp&no=${mood.eno }&name=${mood.ena }&sex=${mood.sex }&dept=${mood.edept }&entryTime=${mood.etime }" target="contentPage"><img alt="" src="img/edit.png" class="operateImg"></a>
								<a href="emp/showInfo.do?no=${mood.eno }&name=${mood.ena }&sex${mood.sex }&dept=${mood.edept }&entryTime=${mood.etime }" target="contentPage"><img alt="" src="img/detail.png" class="operateImg"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot></tfoot>
         	</table>
         	<center>
			<a href="emp/getSomeMood.do?currentPage=1">首页</a> &nbsp;
			<c:if test="${currentPage==1}" var="pre">
				<a href="emp/getSomeMood.do?currentPage=1">上一页</a> &nbsp;
			</c:if>
			<c:if test="${ ! pre }">
				<a href="emp/getSomeMood.do?currentPage=${currentPage-1 }">上一页</a> &nbsp;
			</c:if>
			<c:if test="${currentPage==totalCount}" var="next">
				<a href="emp/getSomeMood.do?currentPage=${totalCount }">下一页</a>&nbsp;
			</c:if>
			<c:if test="${ ! next }">
				<a href="emp/getSomeMood.do?currentPage=${currentPage+1 }">下一页</a>&nbsp;
			</c:if>
			<a href="emp/getSomeMood.do?currentPage=${totalCount }">末 页</a><br />
			一共${totalCount }页,一共${total }条
			</center>
  </body>  
</html>
