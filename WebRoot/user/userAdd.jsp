<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>账户添加</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		body,div,table,tr,td{
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable{
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
			
			
		}
		
		#deptEditTable td{
			height: 40px;
		}
	
	</style>
  </head>
  
  <body>
   	<form action="user/insert.do?from=userAdd" method="post" id="form1">
   	
   	<table id = "deptEditTable">
   		<tr>
   			<td>
   				账号:
   			</td>
   			<td>
   				<input type = "text" name="userNo" id="userNo"/>
   			</td>
   		</tr>
   		<tr>
   			<td>
   				员工编码:
   			</td>
   			<td>
   				<input type = "text" name="empNo" id="empNo"/>
   			</td>
   		</tr>  

   		<tr>
   			<td>
   				员工姓名:
   			</td>
   			<td>
   				<input type = "text" name="empName" id="empName"/>
   			</td>
   		</tr>  

   		<tr>
   			<td>
   				状态:
   			</td>
   			<td>
				<select name="status">
						<option value="1">--请选择--</option>
						<option value="正常">正常</option>
						<option value="注销">注销</option>
					<%-- <c:forEach var="ls" items="${sList }">
    					<option value="${ls.ckey }">${ls.ckey }</option>
         			</c:forEach> --%>
				</select>
   			</td>
		</tr>

		<tr>
			<td>
				角色:
			</td>
			<td>
				<select name="role">
						<option value="1">--请选择--</option>
					<c:forEach var="ls" items="${rList }">
    					<option value="${ls.rname }">${ls.rname }</option>
         			</c:forEach>
				</select>
			</td>
		</tr>
		
   		<tr>
   			<td colspan="2">
   				<input type = "submit" value="添加"/>
   				<input type = "reset" value="重置"/>
				<a href="user/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
   			</td>
   		</tr>  	
   	</table>
   	
   	
   	</form>
  </body>
</html>
