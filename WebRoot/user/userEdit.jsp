<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'userEdit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		body,
		div,
		table,
		tr,
		td {
			margin: 0px;
			padding: 0px;
		}
	
		#deptEditTable {
			font-size: 15px;
			border-collapse: collapse;
			width: 350px;
			margin: 20px auto;
	
	
		}
	
		#deptEditTable td {
			height: 40px;
		}
	</style>
  </head>
  
	<body>
	<form action="user/update.do?from=userEdit" method="post">
		<c:forEach var="uList" items="${uList }">
		<input type="hidden" value="${uList.userId }" name="id" />
		<table id="deptEditTable">
			<tr>
				<td>
					账号:
				</td>
				<td>
					<input type="text" value="${uList.userLogin }" name="userNo" id="deptNo" readonly="readonly"
						style="background-color: #E8E8E8;" />
				</td>
			</tr>
			<tr>
				<td>
					员工编码:
				</td>
				<td>
					<input type="text" value="${uList.userEmpNo }" name="eNo" id="deptName" />
				</td>
			</tr>

			<tr>
				<td>
					员工姓名:
				</td>
				<td>
					<input type="text" value="${uList.userEmpNo }" name="eName" id="deptLoc" />
				</td>
			</tr>

			<tr>
				<td>
					状态:
				</td>
				<td>
					<select name="status">
						<option value="${uList.userStatus }">${uList.userStatus }</option>
						<option value="正常">正常</option>
						<option value="注销">注销</option>
					</select>
				</td>
			</tr>

			<tr>
				<td>
					角色:
				</td>
				<td>
					<select name="role">
						<option value="${uList.userRoleId }">${uList.userRoleId }</option>
						<option value="管理员">管理员</option>
						<option value="普通用户">普通用户</option>
						<option value="人事专员">人事专员</option>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<input type="submit" value="修改" />
					<input type="reset" value="重置" />
					<a href="user/getSomeMood.do" target="contentPage"><input type="button" value="返回"></a>
				</td>
			</tr>
		</table>
		</c:forEach>
	</form>
	</body>
</html>
