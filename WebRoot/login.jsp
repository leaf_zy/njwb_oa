<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'login.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">

   <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
   <style type="text/css">
   body{
   	 background-color: #0070A2;
   }
   </style>
  <!--  <script type="text/javascript">
		$(function(){
			$("#btnSubmit").click(function(){
				var str = $("#userPwd").val();
				$("#userPwd").val(hex_md5(str));//加密 
				//手动提交表单
				var  f = $("#form1");
				f.submit();
			});
		});
	</script> -->
  </head>
  
  <body>
     <div id = "login">
     	  <div id = "title">
     	  		NJWB管理系统
     	  </div>
     	<form action="login.do" method="post" id="form1">
     	  <table id="loginTable">
     	  		<tr>
     	  			<td>用户名:&nbsp;</td>
     	  			<td>
     	  				<input type= "text" name = "userName" id = "userName"/>
     	  			</td>
     	  			<td>&nbsp;</td>
     	  		</tr>
     	  		<tr>
     	  			<td>密&nbsp;&nbsp;&nbsp;码:&nbsp;</td>
     	  			<td>
     	  				<input type= "password" name = "userPwd" id = "userPwd"/>
     	  			</td>
     	  			<td>&nbsp;</td>
     	  		</tr>
      	  		<tr>
     	  			<td>验证码:&nbsp;</td>
     	  			<td>
     	  				<input type= "text" name = "code" id = "code"/>
     	  			</td>
					<td>
						<!-- 这里图片的src已经不是一个固定图片地址，而是一个jsp文件 -->
						<img src="ImageJsp.jsp" id="imgs" title="tupian" alt="tupian" width="60px" height="20px" /> &nbsp; &nbsp; <a
							href="javascript:f_change()">换图片</a>
						<script>
							function f_change() {
								//使用存的js获得img对象，使用jquery框架的 $(),好像不管用
								var img = document.getElementById("imgs");
								img.src = "ImageJsp.jsp?id=" + new Date().getTime();//通过id值的不同，让服务器不是缓存中的图片流，生成一个新的图片流
							}
						</script>
					</td>
					<td>
     	  				&nbsp;
     	  			</td>
     	  		</tr>
     	  		
     	  		<tr>
     	  			<td>&nbsp;</td>
     	  			<td colspan="2">
     	  				<input type= "submit" value="登&nbsp;录" class="btn"/>
     	  			</td>
     	  		</tr>
     	  		    	  		     	  
     	  </table>
     	</form>
     </div>
  </body>
</html>
