package com.wbhz.test;

import com.wbhz.mvc.Action;
import com.wbhz.mvc.MvcConfig;
import com.wbhz.mvc.MyPackage;
import com.wbhz.util.MvcConfigParse;

public class Test004 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MvcConfig config  = MvcConfigParse.getMvcConfig();
		MyPackage packages = config.getPackage("user");
		System.out.println(packages);
		System.out.println("===========================================");
		Action action = packages.getAction("userAction");
		System.out.println(action);
		System.out.println("===========================================");

	}

}
