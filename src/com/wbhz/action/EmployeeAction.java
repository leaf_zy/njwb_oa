package com.wbhz.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.wbhz.entity.Dept;
import com.wbhz.entity.Employee;
import com.wbhz.service.DeptService;
import com.wbhz.service.EmployeeService;

public class EmployeeAction {
	private static Logger logger = Logger.getLogger(EmployeeAction.class);
	EmployeeService employeeService = null;
	DeptService deptService = null;
	// set方法！！
	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	//数据分页
	public String getSomeMood(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		logger.debug("执行EmployeeAction类的getSomeMood方法");
		// 获得部门列表，用于搜索功能（部门下拉框）
		getDeptList(request, response);
		String resultString="success";
		List<Employee> moods=null;
		try {
			moods = employeeService.getAllMoods();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Employee> fenyeMoods = null;
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("list", moods);
		String currPage=request.getParameter("currentPage");
		int size=4;
		//9条数据，显示为三页,+1是处理不满一页的情况
		int totalCount = moods.size()%size==0 ?  moods.size() /size: moods.size()/size+1;
		if(null	!= currPage){
			try {
				fenyeMoods = employeeService.getMoodsPerPage(Integer.parseInt(currPage), size);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", Integer.parseInt(currPage));
		}else{
			try {
				fenyeMoods = employeeService.getMoodsPerPage(1, size);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", "1");//如果是null代表当前页是第一页
		}
		request.setAttribute("some", fenyeMoods);
		request.setAttribute("totalCount",totalCount);
		request.setAttribute("total",moods.size());
		return resultString;
	}
	// 添加
	public String insert(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行EmployeeAction类的insert方法");
		String resultString="";
		String from = request.getParameter("from");// 获得页面来源
		// from: emp或empAdd
		if (from.equals("emp")) {
			getDeptList(request, response);// 获得部门列表，部门下拉框
			return resultString = "toEmpAdd";
		}if (from.equals("empAdd")) {
			String no = request.getParameter("no");
			String name = request.getParameter("name");
			String sex = request.getParameter("sex");
			String dept = request.getParameter("dept");
			String entryTime = request.getParameter("entryTime");
			Employee employee = new Employee();
			employee.setEno(no);
			employee.setEna(name);
			employee.setSex(sex);
			employee.setEdept(dept);
			employee.setEtime(entryTime);
			int count = employeeService.insert(employee);
			if (count==1) {
				getSomeMood(request, response);// 获得数据，toEmp（在emp）页面显示
				resultString = "toEmp";
			}else{
				resultString="fail";
			}
		}
		return resultString;
	}
	// 删除
	public String delete(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行EmployeeAction类的delete方法");
		String resultString="";
		String no = request.getParameter("no");
		Employee employee = new Employee();
		employee.setEno(no);
		int count = employeeService.delete(employee);
		if (count==1) {
			getSomeMood(request, response);// 获得数据，toEmp（在emp）页面显示
			resultString = "toEmp";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 修改
	public String update(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行EmployeeAction类的update方法");
		String resultString="";
		String from = request.getParameter("from");// 获得页面来源
		String no = request.getParameter("no");
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String dept = request.getParameter("dept");
		String entryTime = request.getParameter("entryTime");
		Employee employee = new Employee();
		employee.setEno(no);// 员工编号不可修改，set的原值
		if (from.equals("emp")) {
			getDeptList(request, response);// 获得部门列表，用于部门下拉框
			List<Employee> empList = null;
			empList = employeeService.getEmpListByNo(employee);
			request.setAttribute("emp", empList);
			return resultString = "toEmpEdit";
		}if (from.equals("empEdit")) {
			employee.setEna(name);// set的新值，下同
			employee.setSex(sex);
			employee.setEdept(dept);
			employee.setEtime(entryTime);
			int count = employeeService.update(employee);
			if (count==1) {
				getSomeMood(request, response);// 获得数据，用于emp页面显示
				resultString = "toEmp";
			}else{
				resultString="fail";
			}
		}
		return resultString;
	}
	// 显示信息
	public String showInfo(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行EmployeeAction类的showInfo方法");
		String resultString="";
		String no = request.getParameter("no");
		Employee employee = new Employee();
		employee.setEno(no);
		List<Employee> empList = null;
		empList = employeeService.getEmpListByNo(employee);
		request.setAttribute("emp", empList);
		resultString = "toEmpDetail";
		return resultString;
	}
	// 模糊搜索
	public String search(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行EmployeeAction类的search方法");
		String resultString="";
		String ename = request.getParameter("ename");
		String edept = request.getParameter("edept");
		Employee employee = new Employee();
		employee.setEna(ename);
		employee.setEdept(edept);
		List<Employee> eList = employeeService.selectSomeEmployee(employee);
		System.out.println(eList.toString());
		request.setAttribute("some", eList);
		getDeptList(request, response);// 获得部门列表，用于部门下拉框
		resultString = "toEmp";
		return resultString;
	}
	// 获得部门列表
	public void getDeptList(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		logger.debug("执行EmployeeAction类的getDeptList方法");
		List<Dept> deptList = null;
		try {
			deptList = deptService.getAllMoods();
		} catch (SQLException e1) {
			logger.warn("错误警告：执行EmployeeAction类的getDeptList方法");
			e1.printStackTrace();
		}
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("deptList", deptList);
	}
}
