package com.wbhz.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.wbhz.entity.User;
import com.wbhz.service.UserService;

public class AjaxAction {
	private static Logger logger = Logger.getLogger(AjaxAction.class);
	private UserService userService;
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public String ajax1(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		String resultString = "success";
		String name = request.getParameter("name");
		System.out.println(name);
//		name=URLDecoder.decode(name);
//		System.out.println(name);
		return resultString;
	}
	public String ajax2(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		String resultString = "success";
		String name = request.getParameter("name");
		logger.info("验证用户名是否存在");
		System.out.println(name);
		User user = userService.getUserByLoginName(name);
		if(null==user) {
			resultString = "fail";
		}
		return resultString;
	}
}