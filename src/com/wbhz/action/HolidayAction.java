package com.wbhz.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.wbhz.entity.Holiday;
import com.wbhz.service.HolidayService;

public class HolidayAction {
	private static Logger logger = Logger.getLogger(HolidayAction.class);
	HolidayService holidayService = null;
	// setService
	public void setHolidayService(HolidayService holidayService) {
		this.holidayService = holidayService;
	}
	//数据分页
	public String getSomeMood(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		String resultString="success";
		List<Holiday> moods=null;
		try {
			moods = holidayService.getAllMoods();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Holiday> fenyeMoods = null;
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("list", moods);
		String currPage=request.getParameter("currentPage");
		int size=4;
		//9条数据，显示为三页,+1是处理不满一页的情况
		int totalCount = moods.size()%size==0 ?  moods.size() /size: moods.size()/size+1;
		if(null	!= currPage){
			try {
				fenyeMoods = holidayService.getMoodsPerPage(Integer.parseInt(currPage), size);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", Integer.parseInt(currPage));
		}else{
			try {
				fenyeMoods = holidayService.getMoodsPerPage(1, size);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", "1");//如果是null代表当前页是第一页
		}
		request.setAttribute("some", fenyeMoods);
		request.setAttribute("totalCount",totalCount);
		request.setAttribute("total",moods.size());
		return resultString;
	}
	// 添加
	public String insert(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行HolidayAction类的insert方法");
		String from = request.getParameter("from");
		if (from.equals("holiday")) {
			HttpSession session = request.getSession();
			session.getAttribute("userName");// 获得session属性
			return resultString = "toHolidayAdd";
		}else if (from.equals("holidayAdd")) {
			String name = request.getParameter("name");// 获得申请人姓名
			String type = request.getParameter("type");
			String reason = request.getParameter("hbz");
			String stime = request.getParameter("stime");
			String etime = request.getParameter("etime");
			String status = request.getParameter("sub");
			Holiday holiday = new Holiday();
			holiday.setHuser(name);
			holiday.setHtype(type);
			holiday.setHbz(reason);
			holiday.setStime(stime);
			holiday.setEtime(etime);
			holiday.setHstatus(status);
			int count = holidayService.insert(holiday);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}
		return resultString;
	}
	// 删除
	// 非草稿状态，不允许删除
	public String delete(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行HolidayAction类的delete方法");
		String status = request.getParameter("status");
		if (status.equals("草稿")) {
			// 草稿
			String id = request.getParameter("id");
			Holiday holiday = new Holiday();
			holiday.setId(Integer.parseInt(id));
			int count = holidayService.delete(holiday);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}else if (status.equals("已提交")) {
			// 已提交
			resultString = "submitted";
		}
//		System.out.println("\nstatus: "+status);
		return resultString;
	}
	// 修改
	// "已提交"的数据不允许修改
	public String update(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行HolidayAction类的update方法");
		String from = request.getParameter("from");
		String status = request.getParameter("status");
		if (from.equals("holiday")) {
			if (status.equals("已提交")) {
				resultString = "submitted";
			}else if (status.equals("草稿")) {
				String id = request.getParameter("id");
				Holiday holiday = new Holiday();
				holiday.setId(Integer.parseInt(id));
				List<Holiday> holList = holidayService.getHolList(holiday);
				request.setAttribute("holList", holList);
				resultString = "toHolidayEdit";
			}
		}else if (from.equals("holidayEdit")) {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String type = request.getParameter("type");
			String reason = request.getParameter("reason");
			String sTime = request.getParameter("sTime");
			String eTime = request.getParameter("eTime");
			Holiday holiday = new Holiday();
			holiday.setId(Integer.parseInt(id));
			holiday.setHuser(name);
			holiday.setHtype(type);
			holiday.setHbz(reason);
			holiday.setStime(sTime);
			holiday.setEtime(eTime);
			holiday.setHstatus(status);
			int count = holidayService.update(holiday);
			if (count == 1) {
				resultString = "success";
			}else {
				resultString = "fail";
			}
		}
		return resultString;
	}
	// 显示信息
	public String showInfo(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行HolidayAction类的showInfo方法");
		String id = request.getParameter("id");
		Holiday holiday = new Holiday();
		holiday.setId(Integer.parseInt(id));
		int count = holidayService.showInfo(holiday);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 模糊搜索
	public String search(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行HolidayAction类的search方法");
		String resultString="";
		String hname = request.getParameter("hname");
		String htype = request.getParameter("htype");
		String hstatus = request.getParameter("hstatus");
		List<Holiday> hList = holidayService.getSomeHoliday(hname, htype, hstatus);
		System.out.println("\n hList: "+hList.toString());
		request.setAttribute("some", hList);
		resultString = "success";
		return resultString;
	}
}
