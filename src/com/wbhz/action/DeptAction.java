package com.wbhz.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.wbhz.entity.Dept;
import com.wbhz.entity.Employee;
import com.wbhz.service.DeptService;
import com.wbhz.service.EmployeeService;
import com.wbhz.util.BeanFactory;

public class DeptAction {
	private static Logger logger = Logger.getLogger(UserAction.class);
	DeptService deptService = null;
	
	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}
	//数据分页
	public String getSomeMood(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		String resultString="success";
		List<Dept> moods=null;
		try {
			moods = deptService.getAllMoods();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Dept> fenyeMoods = null;
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("list", moods);
		String currPage=request.getParameter("currentPage");
		int size=4;
		//9条数据，显示为三页,+1是处理不满一页的情况
		int totalCount = moods.size()%size==0 ?  moods.size() /size: moods.size()/size+1;
		if(null	!= currPage){
			try {
				fenyeMoods = deptService.getMoodsPerPage(Integer.parseInt(currPage), size);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", Integer.parseInt(currPage));
		}else{
			try {
				fenyeMoods = deptService.getMoodsPerPage(1, size);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", "1");//如果是null代表当前页是第一页
		}
		request.setAttribute("some", fenyeMoods);
		request.setAttribute("totalCount",totalCount);
		request.setAttribute("total",moods.size());
		return resultString;
	}
	// 添加
	public String insert(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行DeptAction类的insert方法");
		String deptNo = request.getParameter("deptNo");
		String deptName = request.getParameter("deptName");
		String deptLoc = request.getParameter("deptLoc");
		String deptMaster = request.getParameter("deptMaster");
		Dept d = new Dept(deptNo, deptName, deptLoc, deptMaster, null);
		int count = deptService.insertDept(d);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 删除：该部门下还有员工关联，不允许删除！
	public String delete(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行DeptAction类的delete方法");
		String dname = request.getParameter("dname");
		Employee emp = new Employee();
		emp.setEdept(dname);
		EmployeeService empService = (EmployeeService)BeanFactory.getObject("employeeService");
		List<Employee> eList = empService.getEmpListByDept(emp);
		if (eList.toString() == "[]") {
			String deptNo = request.getParameter("no");
			Dept d = new Dept();
			d.setDno(deptNo);
			int count = deptService.delete(d);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}else {
			resultString = "empLink";
		}
		return resultString;
	}
	// 修改
	public String update(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行DeptAction类的update方法");
		String deptNo = request.getParameter("deptNo");
		String deptName = request.getParameter("deptName");
		String deptLoc = request.getParameter("deptLoc");
		String deptMaster = request.getParameter("deptMaster");
		Dept d = new Dept(deptNo, deptName, deptLoc, deptMaster, null);
		int count = deptService.update(d);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 显示信息
	public String showInfo(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行DeptAction类的showInfo方法");
		String deptNo = request.getParameter("deptNo");
		Dept d = new Dept();
		d.setDno(deptNo);
		int count = deptService.showInfo(d);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
}
