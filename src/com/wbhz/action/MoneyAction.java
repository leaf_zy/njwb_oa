package com.wbhz.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.wbhz.entity.Money;
import com.wbhz.service.MoneyService;

public class MoneyAction {
	private static Logger logger = Logger.getLogger(MoneyAction.class);
	MoneyService moneyService = null;
	
	public void setMoneyService(MoneyService moneyService) {
		this.moneyService = moneyService;
	}
	//数据分页
	public String getSomeMood(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		logger.debug("执行MoneyAction类的getSomeMood方法");
		String resultString="success";
		List<Money> moods=null;
		try {
			moods = moneyService.getAllMoods();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Money> fenyeMoods = null;
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("list", moods);
		String currPage=request.getParameter("currentPage");
		int size=4;
		//9条数据，显示为三页,+1是处理不满一页的情况
		int totalCount = moods.size()%size==0 ?  moods.size() /size: moods.size()/size+1;
		if(null	!= currPage){
			try {
				fenyeMoods = moneyService.getMoodsPerPage(Integer.parseInt(currPage), size);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", Integer.parseInt(currPage));
		}else{
			try {
				fenyeMoods = moneyService.getMoodsPerPage(1, size);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", "1");//如果是null代表当前页是第一页
		}
		request.setAttribute("some", fenyeMoods);
		request.setAttribute("totalCount",totalCount);
		request.setAttribute("total",moods.size());
		return resultString;
	}
	// 添加
	public String insert(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行MoneyAction类的insert方法");
		String from = request.getParameter("from");// 获得来源
		if (from.equals("money")) {
			resultString = "toMoneyAdd";
		}else if (from.equals("moneyAdd")) {
			String name = request.getParameter("name");
			String mtype = request.getParameter("mtype");
			String moneyNum = request.getParameter("moneyNum");
			String mbz = request.getParameter("mbz");
			String mstatus = request.getParameter("sub");
			Money money = new Money();
			money.setMuser(name);
			money.setMtype(mtype);
			money.setMoney(Double.parseDouble(moneyNum));
			money.setMbz(mbz);
			money.setMstatus(mstatus);
			int count = moneyService.insert(money);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}
		return resultString;
	}
	// 删除: 非草稿状态，不允许删除
	public String delete(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行MoneyAction类的delete方法");
		String status = request.getParameter("status");
		if (status.equals("草稿")) {
			String id = request.getParameter("id");
			Money money = new Money();
			money.setId(Integer.parseInt(id));
			int count = moneyService.delete(money);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}else if (status.equals("已提交")) {
			// 已提交
			resultString = "submitted";
		}
		return resultString;
	}
	// 修改： "已提交"的数据不允许修改
	public String update(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行MoneyAction类的update方法");
		String from = request.getParameter("from");// 获得来源
		String mstatus = request.getParameter("status");
		if (from.equals("money")) {
			if (mstatus.equals("已提交")) {
				resultString = "submitted";
			}else if (mstatus.equals("草稿")) {
				String id = request.getParameter("id");
				Money money = new Money();
				money.setId(Integer.parseInt(id));
				List<Money> mList = moneyService.getMoneyList(money);
				request.setAttribute("mList", mList);
				resultString = "toMoneyEdit";
			}
		}else if (from.equals("moneyEdit")) {// 修改页面
			String id = request.getParameter("id");
			String mtype = request.getParameter("type");
			String money_1 = request.getParameter("money");
			String mbz = request.getParameter("mbz");
			Money money = new Money();
			money.setId(Integer.parseInt(id));
			money.setMtype(mtype);
			money.setMoney(Double.parseDouble(money_1));
			money.setMstatus(mstatus);
			money.setMbz(mbz);
			int count = moneyService.update(money);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}
		return resultString;
	}
	// 显示信息
	public String showInfo(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行MoneyAction类的showInfo方法");
		String id = request.getParameter("id");
		Money money = new Money();
		money.setId(Integer.parseInt(id));
		int count = moneyService.showInfo(money);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 模糊搜索
	public String search(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		logger.debug("执行MoneyAction类的search方法");
		String resultString="";
		String status = request.getParameter("status");
		String type = request.getParameter("type");
		Money money = new Money();
		money.setMtype(type);
		money.setMstatus(status);
		List<Money> list = moneyService.getSomeMoney(money);
		System.out.println("\n list: "+list.toString());
		request.setAttribute("some", list);
		resultString = "success";
		return resultString;
	}
}
