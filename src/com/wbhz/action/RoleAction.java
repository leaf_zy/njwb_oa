package com.wbhz.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.wbhz.entity.Role;
import com.wbhz.entity.User;
import com.wbhz.service.RoleService;
import com.wbhz.service.UserService;
import com.wbhz.util.BeanFactory;

public class RoleAction {
	private static Logger logger = Logger.getLogger(RoleAction.class);
	RoleService roleService = null;
	
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	//数据分页
	public String getSomeMood(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		logger.debug("执行RoleAction类的getSomeMood方法");
		String resultString="success";
		List<Role> moods=null;
		try {
			moods = roleService.getAllMoods();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Role> fenyeMoods = null;
		//集合获得了，如何让EL表达式可以访问
		request.setAttribute("list", moods);
		String currPage=request.getParameter("currentPage");
		int size=4;
		//9条数据，显示为三页,+1是处理不满一页的情况
		int totalCount = moods.size()%size==0 ?  moods.size() /size: moods.size()/size+1;
		if(null	!= currPage){
			try {
				fenyeMoods = roleService.getMoodsPerPage(Integer.parseInt(currPage), size);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", Integer.parseInt(currPage));
		}else{
			try {
				fenyeMoods = roleService.getMoodsPerPage(1, size);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("currentPage", "1");//如果是null代表当前页是第一页
		}
		request.setAttribute("some", fenyeMoods);
		request.setAttribute("totalCount",totalCount);
		request.setAttribute("total",moods.size());
		return resultString;
	}
	// 添加
	public String insert(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行RoleAction类的insert方法");
		String name = request.getParameter("name");
		Role role = new Role();
		role.setRname(name);
		int count = roleService.insert(role);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
	// 删除: 角色下还有用户关联，则不允许删除
	public String delete(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行RoleAction类的delete方法");
		String rname = request.getParameter("rname");
		User user = new User();
		user.setUserRoleId(rname);
		UserService userService = (UserService)BeanFactory.getObject("userService");
		List<User> uList = userService.getUserListByRole(user);
		if (uList.toString() == "[]") {// 角色下没有用户关联，可删
			String id = request.getParameter("id");
			Role role = new Role();
			role.setId(Integer.parseInt(id));
			int count = roleService.delete(role);
			if (count==1) {
				resultString = "success";
			}else{
				resultString="fail";
			}
		}else {
			resultString="userLink";
		}
		return resultString;
	}
	// 修改
	public String update(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException, SQLException{
		String resultString="";
		logger.debug("执行RoleAction类的update方法");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		Role role = new Role();
		role.setId(Integer.parseInt(id));
		role.setRname(name);
		int count = roleService.update(role);
		if (count==1) {
			resultString = "success";
		}else{
			resultString="fail";
		}
		return resultString;
	}
}
