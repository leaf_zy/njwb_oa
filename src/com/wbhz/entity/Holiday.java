package com.wbhz.entity;

public class Holiday {
	private int id;
	private String hno;
	private String huser;
	private String htype;
	private String hbz;
	private String stime;
	private String etime;
	private String hstatus;
	private String ctime;

	public Holiday() {
		super();
	}

	public Holiday(int id, String hno, String huser, String htype, String hbz, String stime, String etime, String hstatus, String ctime) {
		super();
		this.id = id;
		this.hno = hno;
		this.huser = huser;
		this.htype = htype;
		this.hbz = hbz;
		this.stime = stime;
		this.etime = etime;
		this.hstatus = hstatus;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHno() {
		return hno;
	}

	public void setHno(String hno) {
		this.hno = hno;
	}

	public String getHuser() {
		return huser;
	}

	public void setHuser(String huser) {
		this.huser = huser;
	}

	public String getHtype() {
		return htype;
	}

	public void setHtype(String htype) {
		this.htype = htype;
	}

	public String getHbz() {
		return hbz;
	}

	public void setHbz(String hbz) {
		this.hbz = hbz;
	}

	public String getStime() {
		return stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getHstatus() {
		return hstatus;
	}

	public void setHstatus(String hstatus) {
		if (hstatus.equals("提交") || hstatus.equals("已提交")) {
			this.hstatus = "已提交";
		}else {
			this.hstatus = "草稿";
		}
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Holiday [ctime=" + ctime + ", etime=" + etime + ", hbz=" + hbz + ", hno=" + hno + ", hstatus=" + hstatus + ", htype=" + htype + ", huser=" + huser + ", id=" + id + ", stime=" + stime
				+ "]";
	}

}
