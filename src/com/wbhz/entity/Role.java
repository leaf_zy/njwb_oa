package com.wbhz.entity;

public class Role {
	private int id;
	private String rname;
	private String ctime;

	public Role() {
		super();
	}

	public Role(int id, String rname, String ctime) {
		super();
		this.id = id;
		this.rname = rname;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Role [ctime=" + ctime + ", id=" + id + ", rname=" + rname + "]";
	}

}
