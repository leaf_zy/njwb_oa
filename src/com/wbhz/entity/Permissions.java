package com.wbhz.entity;

public class Permissions {
	private int id;
	private int rid;
	private String rname;
	private int mid;
	private String mname;
	private String ctime;

	public Permissions() {
		super();
	}

	public Permissions(int id, int rid, String rname, int mid, String mname, String ctime) {
		super();
		this.id = id;
		this.rid = rid;
		this.rname = rname;
		this.mid = mid;
		this.mname = mname;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Permissions [ctime=" + ctime + ", id=" + id + ", mid=" + mid + ", mname=" + mname + ", rid=" + rid + ", rname=" + rname + "]";
	}

}
