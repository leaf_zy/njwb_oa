package com.wbhz.entity;

import java.util.List;

public class Page {
	private int nowPage;
	private int totalPage;
	private int totalCount;
	private int pageSize;
	private List<Object> pageList;

	public Page() {
		super();
	}

	public Page(int nowPage, int totalPage, int totalCount, int pageSize, List<Object> pageList) {
		super();
		this.nowPage = nowPage;
		this.totalPage = totalPage;
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.pageList = pageList;
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getTotalPage() {
		totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<Object> getPageList() {
		return pageList;
	}

	public void setPageList(List<Object> pageList) {
		this.pageList = pageList;
	}

	@Override
	public String toString() {
		return "Page [nowPage=" + nowPage + ", pageList=" + pageList + ", pageSize=" + pageSize + ", totalCount=" + totalCount + ", totalPage=" + totalPage + "]";
	}

}
