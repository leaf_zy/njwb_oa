package com.wbhz.entity;

public class Menu {
	private int id;
	private String mname;
	private String hurl;
	private String pid;
	private String ctime;

	public Menu() {
		super();
	}

	public Menu(int id, String mname, String hurl, String pid, String ctime) {
		super();
		this.id = id;
		this.mname = mname;
		this.hurl = hurl;
		this.pid = pid;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getHurl() {
		return hurl;
	}

	public void setHurl(String hurl) {
		this.hurl = hurl;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Menu [ctime=" + ctime + ", hurl=" + hurl + ", id=" + id + ", mname=" + mname + ", pid=" + pid + "]";
	}

}
