package com.wbhz.entity;

import java.util.Date;

public class User {
	private int userId;//用户编号
	private String userLogin;//登录名
	private String userPwd;//密码
	private String userEmpNo;
	private String userRoleId;
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	private Date userDate;//创建时间
	private String userStatus;
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userLogin=" + userLogin + ", userPwd=" + userPwd + ", userEmpNo="
				+ userEmpNo + ", userRoleId=" + userRoleId + ", userDate=" + userDate + ", userStatus=" + userStatus
				+ "]";
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getUserEmpNo() {
		return userEmpNo;
	}
	public void setUserEmpNo(String userEmpNo) {
		this.userEmpNo = userEmpNo;
	}
	public Date getUserDate() {
		return userDate;
	}
	public void setUserDate(Date userDate) {
		this.userDate = userDate;
	}

}
