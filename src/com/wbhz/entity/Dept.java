package com.wbhz.entity;

public class Dept {
	private String dno;
	private String dname;
	private String dloc;
	private String dmanager;
	private String ctime;

	public Dept() {
		super();
	}

	public Dept(String dno, String dname, String dloc, String dmanager, String ctime) {
		super();
		this.dno = dno;
		this.dname = dname;
		this.dloc = dloc;
		this.dmanager = dmanager;
		this.ctime = ctime;
	}

	public String getDno() {
		return dno;
	}

	public void setDno(String dno) {
		this.dno = dno;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getDloc() {
		return dloc;
	}

	public void setDloc(String dloc) {
		this.dloc = dloc;
	}

	public String getDmanager() {
		return dmanager;
	}

	public void setDmanager(String dmanager) {
		this.dmanager = dmanager;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Dept [ctime=" + ctime + ", dloc=" + dloc + ", dmanager=" + dmanager + ", dname=" + dname + ", dno=" + dno + "]";
	}
}
