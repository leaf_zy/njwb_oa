package com.wbhz.entity;

public class Money {
	private int id;
	private String mon;
	private String muser;
	private String mtype;
	private double money;
	private String ctime;
	private String mstatus;
	private String mbz;

	public Money() {
	}

	public Money(int id, String mon, String muser, String mtype, double money, String ctime, String mstatus, String mbz) {
		super();
		this.id = id;
		this.mon = mon;
		this.muser = muser;
		this.mtype = mtype;
		this.money = money;
		this.ctime = ctime;
		this.mstatus = mstatus;
		this.mbz = mbz;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMbz() {
		return mbz;
	}

	public void setMbz(String mbz) {
		this.mbz = mbz;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getMuser() {
		return muser;
	}

	public void setMuser(String muser) {
		this.muser = muser;
	}

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getMstatus() {
		return mstatus;
	}

	public void setMstatus(String mstatus) {
		if (mstatus.equals("提交") || mstatus.equals("已提交")) {
			this.mstatus = "已提交";
		}else {
			this.mstatus = "草稿";
		}
	}

	@Override
	public String toString() {
		return "Money [ctime=" + ctime + ", id=" + id + ", mbz=" + mbz + ", mon=" + mon + ", money=" + money + ", mstatus=" + mstatus + ", mtype=" + mtype + ", muser=" + muser + "]";
	}

}
