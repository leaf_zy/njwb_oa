package com.wbhz.entity;

public class SysConfig {
	private int id;
	private String ctype;
	private String ckey;
	private String pvalue;
	private String ctime;

	public SysConfig() {
		super();
	}

	public SysConfig(int id, String ctype, String ckey, String pvalue, String ctime) {
		super();
		this.id = id;
		this.ctype = ctype;
		this.ckey = ckey;
		this.pvalue = pvalue;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getCkey() {
		return ckey;
	}

	public void setCkey(String ckey) {
		this.ckey = ckey;
	}

	public String getPvalue() {
		return pvalue;
	}

	public void setPvalue(String pvalue) {
		this.pvalue = pvalue;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "SysConfig [ckey=" + ckey + ", ctime=" + ctime + ", ctype=" + ctype + ", id=" + id + ", pvalue=" + pvalue + "]";
	}

}
