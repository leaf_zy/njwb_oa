package com.wbhz.entity;

public class Employee {
	private int id;
	private String eno;
	private String ena;
	private String edept;
	private String sex;
	private String edu;
	private String email;
	private String phone;
	private String etime;
	private String ctime;

	public Employee() {
		super();
	}

	public Employee(int id, String eno, String ena, String edept, String sex, String edu, String email, String phone, String etime, String ctime) {
		super();
		this.id = id;
		this.eno = eno;
		this.ena = ena;
		this.edept = edept;
		this.sex = sex;
		this.edu = edu;
		this.email = email;
		this.phone = phone;
		this.etime = etime;
		this.ctime = ctime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEno() {
		return eno;
	}

	public void setEno(String eno) {
		this.eno = eno;
	}

	public String getEna() {
		return ena;
	}

	public void setEna(String ena) {
		this.ena = ena;
	}

	public String getEdept() {
		return edept;
	}

	public void setEdept(String edept) {
		this.edept = edept;
	}

	public String getSex() {
		return sex;
	}
	
	public String getSex2() {
		if ("男".equals(sex)) {
			return "1";
		} else {
			return "0";
		}
	}
	// 性别，0：女，1：男
	public void setSex(String sex) {
		if ("1".equals(sex)) {
			this.sex = "男";
		} else {
			this.sex = "女";
		}
	}

	public String getEdu() {
		return edu;
	}

	public void setEdu(String edu) {
		this.edu = edu;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	@Override
	public String toString() {
		return "Employee [ctime=" + ctime + ", edept=" + edept + ", edu=" + edu + ", email=" + email + ", ena=" + ena + ", eno=" + eno + ", etime=" + etime + ", id=" + id + ", phone=" + phone
				+ ", sex=" + sex + "]";
	}

}
