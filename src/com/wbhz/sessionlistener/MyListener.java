package com.wbhz.sessionlistener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MyListener implements HttpSessionListener {

	//https://blog.csdn.net/qq_32444825/article/details/89485547
	//也可以处理聊天室，**加入，**退出
	// 图书144页
	public void sessionCreated(HttpSessionEvent arg0) {
		// 使用监听器处理访问人数问题，一个session的创建就对应一个人的访问，而且访问人数是对所有人可见
		System.out.println("session已经创建");
		//获得Application级的变量作用域
		ServletContext appl=arg0.getSession().getServletContext();
		Object obj = appl.getAttribute("count");
		if(null==obj){
			//代表Application级的变量作用域中不存在count键对应的值，count键对应的值为1（网站第一被用户访问）
			appl.setAttribute("count",1);
		}else{
			int count = (Integer)obj;
			count++;//人数加一
			appl.setAttribute("count",count);
			System.out.println(count);
		}

	}

	public void sessionDestroyed(HttpSessionEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("session已经销毁");
	}

}
