package com.wbhz.sessionlistener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MyHttpSessionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent arg0) {
		// https://www.cnblogs.com/shenbing/p/5372717.html
		ServletContext application = arg0.getSession().getServletContext();
		Object obj = application.getAttribute("count");
		if(obj!=null){
			int count = (Integer)obj;
			System.out.println("创建后count:" + count);
		}
		// 获得application
		System.out.println("session对象被创建了");
		HttpSession session = arg0.getSession();

		System.out.println("id" + session.getId());

	}

	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("session对象被销毁");
		ServletContext application = arg0.getSession().getServletContext();
		int count = ((Integer) application.getAttribute("count")).intValue();
		count = count - 1;
		application.setAttribute("count", count);
		System.out.println("销毁后的count:" + count);
		HttpSession session = arg0.getSession();
		System.out.println("id" + session.getId());

	}

}
