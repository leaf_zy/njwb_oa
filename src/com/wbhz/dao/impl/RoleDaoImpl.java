package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.RoleDao;
import com.wbhz.dao.mapper.RoleMapper;
import com.wbhz.entity.Role;
import com.wbhz.util.JdbcTemplate;

public class RoleDaoImpl implements RoleDao {
	private static Logger logger = Logger.getLogger(RoleDaoImpl.class);

	public int insert(Role r) throws SQLException {
		logger.debug("在RoleDaoImpl类中，调用insert方法");
		String sql = "insert into t_role(t_role_name,t_create_time) values (?,now())";
		String[] objects = {r.getRname()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert返回结果是："+count);
		return count;
	}
	// 如果该角色下还有用户关联，则不允许删除，弹出框提示"该角色下还有用户关联，不允许删除！"
	public int delete(Role r)  throws SQLException {
		logger.debug("在RoleDaoImpl类中，调用delete方法");
		String sql = "delete from t_role where id = ?";
		String[] objects = {r.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}
	// 角色ID不允许修改
	public int update(Role r) throws SQLException{
		logger.debug("在RoleDaoImpl类中，调用update方法");
		String sql = "update t_role set t_role_name = ? where id = ?";
		String[] objects = {r.getRname(), r.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}
	
	public List<Role> getAllMoods() throws SQLException {
		logger.debug("在RoleDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_role";
		return JdbcTemplate.executeQuery(sql, new RoleMapper(), null);
	}
	
	public List<Role> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在RoleDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_role LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new RoleMapper(), null);
	}

}
