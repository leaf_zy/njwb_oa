package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.PermissionsDao;
import com.wbhz.dao.mapper.PermissionsMapper;
import com.wbhz.entity.Permissions;
import com.wbhz.util.JdbcTemplate;

public class PermissionsDaoImpl implements PermissionsDao {
	private static Logger logger = Logger.getLogger(PermissionsDaoImpl.class);

	public int insert(Permissions p) throws SQLException {
		logger.debug("在PermissionsDaoImpl类中，调用insert方法");
		String sql = "insert into t_permissions(t_role_id,t_menu_id,t_create_time) values (?,?,now())";
		String[] objects = {p.getRid()+"", p.getMid()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert返回结果是："+count);
		return count;
	}
	
	public int delete(Permissions p)  throws SQLException {
		logger.debug("在PermissionsDaoImpl类中，调用delete方法");
		String sql = "delete from t_permissions where id = ?";
		String[] objects = {p.getRid()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}
	
	public int update(Permissions p) throws SQLException{
		logger.debug("在PermissionsDaoImpl类中，调用update方法");
		String sql = "update t_permissions set t_role_id = ?,t_menu_id = ? where id = ?";
		String[] objects = {p.getRid()+"", p.getMid()+"", p.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}
	
	public List<Permissions> getAllMoods() throws SQLException {
		logger.debug("在PermissionsDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_permissions";
		return JdbcTemplate.executeQuery(sql, new PermissionsMapper(), null);
	}
	
	public List<Permissions> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在PermissionsDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_permissions LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new PermissionsMapper(), null);
	}

}
