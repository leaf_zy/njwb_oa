package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.UserDao;
import com.wbhz.dao.mapper.UserMapper;
import com.wbhz.entity.User;
import com.wbhz.util.JdbcTemplate;

public class UserDaoImpl implements UserDao {
	private static Logger logger = Logger.getLogger(UserDaoImpl.class);
	//格式化日期
//	private static SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
	public int insert(User user)  throws SQLException {
		logger.debug("在UserDaoImpl类中，调用insert方法");
		String sql="insert into t_user(t_user_account, t_user_pwd, t_emp_no, t_role_id, t_user_status, t_create_time)values(?,?,?,?,?,now())";
		String[] objects = {user.getUserLogin(), user.getUserLogin(), user.getUserEmpNo(), user.getUserRoleId(), user.getUserStatus()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert方法的返回结果是："+count);
		return count;
	}

	public int update(User user)  throws SQLException{
		logger.debug("在UserDaoImpl类中，调用update方法");
		String sql="UPDATE t_user set t_emp_no=?, t_role_id=?, t_user_status=? where id=?";
		String[] objects = {user.getUserEmpNo(), user.getUserRoleId(), user.getUserStatus(), user.getUserId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}

	public int delete(User user)  throws SQLException {
		logger.debug("在UserDaoImpl类中，调用delete方法");
		String sql="delete from t_user where id=?";//所有的占位符必须是英文的问号
		String[] objects = {user.getUserId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}

	public List<User> getUserById(int id) {
		logger.debug("在UserDaoImpl类中，调用getUserById方法");
		String sql = "select * from t_user where id=?";
		String[] objects={id+""};
		List<User> list =JdbcTemplate.executeQuery(sql, new UserMapper(), objects);
//		if(list.size()>0){
//			return list.get(0); //查到了
//		}
//		return null;//没有查到
		return list;
	}
	
	public List<User> getAllUsersByCondition(User user) {
		logger.debug("在UserDaoImpl类中，调用getAllUsersByCondition方法");
		String sql = "select * from t_user where t_user_account like \"%\"?\"%\" and t_user_status=? and t_role_id=?";
		String[] objects={user.getUserLogin(), user.getUserStatus(), user.getUserRoleId()};
		List<User> list =JdbcTemplate.executeQuery(sql, new UserMapper(), objects);
		return list;
	}
//
//	public List<User> getAllUsers() {
//		logger.debug("在UserDaoImpl类中，调用getAllUsers方法");
//		String sql = "select user_id,user_login,user_pwd,user_date,user_score,user_hobby from t_user";
//		return JdbcTemplate.executeQuery(sql, new UserMapper(), null);
//	}

	//登录功能
	public User login(User user) {
		User userResult = null;
		logger.debug("在UserDaoImpl类中，调用login方法");
		String sql = "select id, t_user_account, t_user_pwd, t_emp_no, t_role_id, t_create_time, t_user_status from t_user where t_user_account=? and t_user_pwd=?";
		Object[] objects = {user.getUserLogin(),user.getUserPwd()};
		List<User> users = JdbcTemplate.executeQuery(sql, new UserMapper(), objects);
		if(null!=users && users.size()==1){
			userResult = users.get(0);
		}
		return userResult;
	}
	
	public User getUserByLoginName(String loginName) {
		logger.debug("在UserDaoImpl类中，调用getUserByLoginName方法");
		String sql = "select id, t_user_account, t_user_pwd, t_emp_no, t_role_id, t_create_time, t_user_status from t_user where t_user_account=?";
		String[] objects={loginName};
		List<User> list = JdbcTemplate.executeQuery(sql, new UserMapper(), objects);
		if(list.size()>0){
			return list.get(0); //查到了
		}
		return null;//没有查到
	}
	
	public List<User> getUserListByRole(User user) throws SQLException {
		logger.debug("在UserDaoImpl类中，调用getUserListByRole方法");
		String sql="select * from t_user where t_role_id=?";
		return JdbcTemplate.executeQuery(sql, new UserMapper(), user.getUserRoleId());
	}
	
	public List<User> getAllMoods() throws SQLException {
		logger.debug("在UserDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_user";
		return JdbcTemplate.executeQuery(sql, new UserMapper(), null);
	}
	
	public List<User> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在UserDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_user LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new UserMapper(), null);
	}
}












