package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.MoneyDao;
import com.wbhz.dao.mapper.MoneyMapper;
import com.wbhz.entity.Money;
import com.wbhz.util.JdbcTemplate;

public class MoneyDaoImpl implements MoneyDao {
	private static Logger logger = Logger.getLogger(MoneyDaoImpl.class);

	public int insert(Money m) throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用insert方法");
		String sql = " insert into t_money(t_money_user, t_money_type, t_money_money, t_money_time, t_money_status, t_money_bz) values (?,?,?,now(),?,?)";
		String[] objects = {m.getMuser(), m.getMtype(), m.getMoney()+"", m.getMstatus(), m.getMbz()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert返回结果是："+count);
		return count;
	}
	
	public int delete(Money m)  throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用delete方法");
		String sql = "delete from t_money where id = ?";
		String[] objects = {m.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}
	
	public int update(Money m) throws SQLException{
		logger.debug("在MoneyDaoImpl类中，调用update方法");
		String sql = "update t_money set t_money_type = ?,t_money_money = ?,t_money_status = ?,t_money_bz = ? where id = ?";
		String[] objects = {m.getMtype(), m.getMoney()+"", m.getMstatus(), m.getMbz(), m.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}
	
	public int showInfo(Money m) throws SQLException{
		logger.debug("在MoneyDaoImpl类中，调用update方法");
		String sql="select * from t_money where id=?";
		String[] objects = {m.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行showInfo返回结果是："+count);
		return count;
	}
	// 模糊搜索
	public List<Money> getSomeMoney(Money money) throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用getSomeMoney方法");
		String sql="select * from t_money where  t_money_type=? and t_money_status=?";
		return JdbcTemplate.executeQuery(sql, new MoneyMapper(), money.getMtype(), money.getMstatus());
	}
	public List<Money> getMoneyList(Money money) throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用getMoneyList方法");
		String sql="select * from t_money where id = ?";
		return JdbcTemplate.executeQuery(sql, new MoneyMapper(), money.getId());
	}
	public List<Money> getAllMoods() throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_money";
		return JdbcTemplate.executeQuery(sql, new MoneyMapper(), null);
	}
	
	public List<Money> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在MoneyDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_money LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new MoneyMapper(), null);
	}

}
