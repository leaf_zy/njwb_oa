package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.HolidayDao;
import com.wbhz.dao.mapper.HolidayMapper;
import com.wbhz.entity.Holiday;
import com.wbhz.util.JdbcTemplate;

public class HolidayDaoImpl implements HolidayDao {
	private static Logger logger = Logger.getLogger(HolidayDaoImpl.class);

	public int insert(Holiday holiday) throws SQLException {
		logger.debug("在HolidayDaoImpl类中，调用insert方法");
		String sql = "insert into t_holiday(t_holiday_user,t_holiday_type,t_holiday_bz,t_start_time,t_end_time,t_holiday_status,t_create_time) values  (?,?,?,?,?,?,now());";
		String[] objects = {holiday.getHuser(), holiday.getHtype(), holiday.getHbz(), holiday.getStime(), holiday.getEtime(), holiday.getHstatus()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert返回结果是："+count);
		return count;
	}
	
	public int delete(Holiday holiday)  throws SQLException {
		logger.debug("在HolidayDaoImpl类中，调用delete方法");
		String sql = "delete from t_holiday where id = ?";
		String[] objects = {holiday.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}
	
	public int update(Holiday holiday) throws SQLException{
		logger.debug("在HolidayDaoImpl类中，调用update方法");
		String sql = " update t_holiday set t_holiday_type = ?,t_holiday_bz = ?,t_start_time = ?,t_end_time = ?,t_holiday_status = ? where id = ?";
		String[] objects = {holiday.getHtype(), holiday.getHbz(), holiday.getStime(), holiday.getEtime(), holiday.getHstatus(), holiday.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}
	
	public int showInfo(Holiday holiday) throws SQLException{
		logger.debug("在HolidayDaoImpl类中，调用update方法");
		String sql="select * from t_holiday where id=?";
		String[] objects = {holiday.getId()+""};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行showInfo返回结果是："+count);
		return count;
	}
	
	public List<Holiday> getHolList(Holiday holiday) throws SQLException {
		logger.debug("在HolidayDaoImpl类中，调用getHolList方法");
		String sql="select * from t_holiday where id=?";
		return JdbcTemplate.executeQuery(sql, new HolidayMapper(), holiday.getId());
	}
	// 模糊搜索
	public List<Holiday> getSomeHoliday(String name, String type, String status) throws SQLException {
//		String sql = "select t_holiday.id,t_holiday_no,t_holiday_user,t_config_page_value,t_holiday_bz,t_start_time,t_end_time,t_holiday_status,t_holiday.t_create_time "
//				+ "from t_holiday,t_sys_config "
//				+ "where t_holiday_type = t_config_key and t_config_type = \"请假\" and t_holiday_user like \"%\"?\"%\" and t_holiday_user = ? and t_config_page_value = ? and t_holiday_status = ?";
		String sql = "select * from t_holiday where t_holiday_user like \"%\"?\"%\" and t_holiday_type=? and t_holiday_status=?";
		return JdbcTemplate.executeQuery(sql, new HolidayMapper(), name, type, status);
	}
	
	public List<Holiday> getAllMoods() throws SQLException {
		logger.debug("在HolidayDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_holiday";
		return JdbcTemplate.executeQuery(sql, new HolidayMapper(), null);
	}
	
	public List<Holiday> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在HolidayDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_holiday LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new HolidayMapper(), null);
	}

}
