package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.dao.SysConfigDao;
import com.wbhz.dao.mapper.SysConfigMapper;
import com.wbhz.entity.SysConfig;
import com.wbhz.util.JdbcTemplate;

public class SysConfigDaoImpl implements SysConfigDao{

	public List<SysConfig> getAllOneSysConfig() throws SQLException {
		String sql = "select * from t_sys_config where t_config_type = \"请假\"";
		return JdbcTemplate.executeQuery(sql, new SysConfigMapper());
	}

	public List<SysConfig> getAllTwoSysConfig() throws SQLException {
		String sql = "select * from t_sys_config where t_config_type = \"报销\"";
		return JdbcTemplate.executeQuery(sql, new SysConfigMapper());
	}

	public List<SysConfig> getAllThressSysConfig() throws SQLException {
		String sql = "select * from t_sys_config where t_config_type = \"帐号\"";
		return JdbcTemplate.executeQuery(sql, new SysConfigMapper());
	}

}
