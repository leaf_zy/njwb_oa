package com.wbhz.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.EmployeeDao;
import com.wbhz.dao.mapper.EmployeeMapper;
import com.wbhz.entity.Employee;
import com.wbhz.util.JdbcTemplate;

public class EmployeeDaoImpl implements EmployeeDao {
	private static Logger logger = Logger.getLogger(EmployeeDaoImpl.class);

	public int insert(Employee e) throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用insert方法");
//		String sql = "insert into t_employee(t_emp_no, t_emp_na, t_emp_dept, t_sex, t_education, t_email,t_phone, t_entry_time, t_create_time) values (?,?,?,?,?,?,?,?,now())";
//		String[] objects = {e.getEno(), e.getEna(), e.getEdept(), e.getSex(), e.getEdu(), e.getEmail(), e.getPhone(), e.getEtime()};
		String sql = "insert into t_employee(t_emp_no, t_emp_na, t_emp_dept, t_sex, t_entry_time, t_create_time) values (?,?,?,?,?,now())";
		String[] objects = {e.getEno(), e.getEna(), e.getEdept(), e.getSex2(), e.getEtime()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行insert返回结果是："+count);
		return count;
	}
	
	public int delete(Employee e)  throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用delete方法");
		String sql = "delete from t_employee where t_emp_no = ?";
		String[] objects = {e.getEno()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行delete返回结果是："+count);
		return count;
	}
	
	public int update(Employee e) throws SQLException{
		logger.debug("在EmployeeDaoImpl类中，调用update方法");
		String sql = "update t_employee set t_emp_na = ?,t_emp_dept = ?,t_sex = ?,t_entry_time = ? where t_emp_no = ?";
		String[] objects = {e.getEna(), e.getEdept(), e.getSex2(), e.getEtime(), e.getEno()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行update返回结果是："+count);
		return count;
	}
	
	public int showInfoByNo(Employee e) throws SQLException{
		logger.debug("在EmployeeDaoImpl类中，调用showInfo方法");
		String sql="select * from t_employee where t_emp_no=?";
		String[] objects = {e.getEno()};
		int count = JdbcTemplate.executeUpdate(sql, objects);
		logger.info("执行showInfo返回结果是："+count);
		return count;
	}
	
	public List<Employee> getEmpListByDept(Employee e) throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用getEmpListByDept方法");
		String sql="select * from t_employee where t_emp_dept=?";
		String[] objects = {e.getEdept()};
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), objects);
	}
	
	public List<Employee> getEmpListByNo(Employee e) throws SQLException{
		logger.debug("在EmployeeDaoImpl类中，调用getEmpListByNo方法");
		String sql="select * from t_employee where t_emp_no=?";
		String[] objects = {e.getEno()};
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), objects);
	}
	public List<Employee> getEmpListByNoAndName(Employee e) throws SQLException{
		logger.debug("在EmployeeDaoImpl类中，调用getEmpListByNoAndName方法");
		String sql="select * from t_employee where t_emp_no=? and t_emp_na=?";
		String[] objects = {e.getEno(), e.getEna()};
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), objects);
	}
	// 模糊搜索
	public List<Employee> selectSomeEmployee(Employee e) throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用selectSomeEmployee方法");
		String sql = "select * from t_employee,t_dept "
				+ "where t_dept.t_dept_name = t_emp_dept and t_emp_na like \"%\"?\"%\" and t_emp_dept = ?";
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), e.getEna(), e.getEdept());
	}
	
	public List<Employee> getAllMoods() throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用getAllMoods方法");
		String sql="select * from t_employee";
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), null);
	}
	
	public List<Employee> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在EmployeeDaoImpl类中，调用getMoodsPerPage方法");
		
		int begin = (currentPage-1)*size; //当前页-1 再乘以显示的大小
		//如果使用数据分页，只能使用字符串拼接的方式
		String sql="select * from t_employee LIMIT "+begin+" , "+size;
		//limit 不支持占位符
		//String sql="select * from t_mood LIMIT ?,?";
		//String[] objs= {begin+"",size+""};
		return JdbcTemplate.executeQuery(sql, new EmployeeMapper(), null);
	}

}
