package com.wbhz.dao;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.User;

public interface UserDao {
	//简单的五个方法，对t_user表的增删改查（查询一个，查询全部)，查询部分--根据条件） 
	public int insert(User user) throws SQLException;
	public int update(User user) throws SQLException;
	public int delete(User user) throws SQLException;
	public List<User> getAllUsersByCondition(User user);
	public List<User> getUserById(int id);
	public User getUserByLoginName(String loginName);
	public List<User> getUserListByRole(User user) throws SQLException;
//	public List<User> getAllUsers();
	//登录功能
	/*
	 * (1)传入用户名，获得密码，在程序比较密码是否正确，这个好一点防止sql脚本注入（一把出现在使用String去拼接sql语句），
	 * 因为我们已经是preparedStatement也就不存在注入的问题，
	 * (2)传入用户名与密码，通过数据库查找符合用户名和密码的行，返回查询结果
	 * */
	public User login(User user);
	//获得全部的心情
	List<User> getAllMoods() throws SQLException; 
	//int currentPage--当前页,int size--每一页显示的条数
	List<User> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
