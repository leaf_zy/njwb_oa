package com.wbhz.dao;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Dept;

public interface DeptDao {
	public int insertDept(Dept d) throws SQLException;
	public int delete(Dept d) throws SQLException;
	public int update(Dept d) throws SQLException;
	public int showInfo(Dept d) throws SQLException;
	//获得全部的心情
	List<Dept> getAllMoods() throws SQLException; 
	//int currentPage--当前页,int size--每一页显示的条数
	List<Dept> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
