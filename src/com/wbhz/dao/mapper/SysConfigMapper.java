package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.SysConfig;
import com.wbhz.util.RowMapper;

public class SysConfigMapper implements RowMapper<SysConfig> {
	private static Logger logger = Logger.getLogger(SysConfigMapper.class);
	
	public List<SysConfig> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_sys_config表中数据");
		List<SysConfig> list = new ArrayList<SysConfig>();
		while (rs.next()) {
			SysConfig sysConfig = new SysConfig();
			sysConfig.setId(rs.getInt("id"));
			sysConfig.setCtype(rs.getString("t_config_type"));
			sysConfig.setCkey(rs.getString("t_config_key"));
			sysConfig.setPvalue(rs.getString("t_config_page_value"));
			sysConfig.setCtime(rs.getString("t_create_time"));
			list.add(sysConfig);
		}
		logger.debug("SysConfigMapper数据分装成功");
		return list;
	}
}
