package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Dept;
import com.wbhz.util.RowMapper;

public class DeptMapper implements RowMapper<Dept> {
	private static Logger logger = Logger.getLogger(DeptMapper.class);
	
	public List<Dept> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_dept表中数据");
		List<Dept> list = new ArrayList<Dept>();
		while (rs.next()) {
			Dept d = new Dept();
			d.setDno(rs.getString("t_dept_no"));
			d.setDname(rs.getString("t_dept_name"));
			d.setDloc(rs.getString("t_dept_loc"));
			d.setDmanager(rs.getString("t_dept_manager"));
			d.setCtime(rs.getString("t_create_time"));
			list.add(d);
		}
		logger.debug("DeptMapper数据分装成功");
		return list;
	}
}
