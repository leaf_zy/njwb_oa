package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Role;
import com.wbhz.util.RowMapper;

public class RoleMapper implements RowMapper<Role> {
	private static Logger logger = Logger.getLogger(RoleMapper.class);
	
	public List<Role> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_role表中数据");
		List<Role> list = new ArrayList<Role>();
		while (rs.next()) {
			Role role = new Role();
			role.setId(rs.getInt("id"));
			role.setRname(rs.getString("t_role_name"));
			role.setCtime(rs.getString("t_create_time"));
			list.add(role);// ！！忘加了
		}
		logger.debug("RoleMapper数据分装成功");
		return list;
	}
}
