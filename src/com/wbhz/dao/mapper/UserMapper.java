package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.User;
import com.wbhz.util.RowMapper;


public class UserMapper implements RowMapper<User> {
	private static SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
	private static Logger logger = Logger.getLogger(UserMapper.class);
	public List<User> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_user表中数据，这是t_user表与User类的映射");
		List<User> list = new ArrayList<User>();
		//数据封装 rs是一个只读只进，只读--不能用于修改，只进--不能回头
		while(rs.next()){
			User user = new User();
			user.setUserId(rs.getInt("id"));
			user.setUserLogin(rs.getString("t_user_account"));
			user.setUserPwd(rs.getString("t_user_pwd"));
			user.setUserEmpNo(rs.getString("t_emp_no"));
			user.setUserRoleId(rs.getString("t_role_id"));
			user.setUserStatus(rs.getString("t_user_status"));
			try {
				user.setUserDate(format.parse(rs.getString("t_create_time")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			list.add(user);
		}
		logger.debug("UserMapper数据分装成功");
		return list;
	}

}
