package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Menu;
import com.wbhz.util.RowMapper;

public class MenuMapper implements RowMapper<Menu> {
	private static Logger logger = Logger.getLogger(MenuMapper.class);
	
	public List<Menu> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_menu表中数据");
		List<Menu> list = new ArrayList<Menu>();
		while (rs.next()) {
			Menu m = new Menu();
			m.setId(rs.getInt("id"));
			m.setMname(rs.getString("t_menu_name"));
			m.setHurl(rs.getString("t_href_url"));
			m.setPid(rs.getString("t_parent_id"));
			m.setCtime(rs.getString("t_create_time"));
			list.add(m);
		}
		logger.debug("MenuMapper数据分装成功");
		return list;
	}
}
