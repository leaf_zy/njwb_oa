package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Permissions;
import com.wbhz.util.RowMapper;

public class PermissionsMapper implements RowMapper<Permissions> {
	private static Logger logger = Logger.getLogger(PermissionsMapper.class);
	
	public List<Permissions> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_permissions表中数据");
		List<Permissions> list = new ArrayList<Permissions>();
		while (rs.next()) {
			Permissions p = new Permissions();
			p.setId(rs.getInt("id"));
			p.setMid(rs.getInt("t_menu_id"));
//			p.setMname(rs.getString("t_menu_name"));
			p.setRid(rs.getInt("t_role_id"));
//			p.setRname(rs.getString("t_role_name"));
			p.setCtime(rs.getString("t_create_time"));
			list.add(p);
		}
		logger.debug("PermissionsMapper数据分装成功");
		return list;
	}
}
