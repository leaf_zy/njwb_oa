package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Employee;
import com.wbhz.util.RowMapper;

public class EmployeeMapper implements RowMapper<Employee> {
	private static Logger logger = Logger.getLogger(EmployeeMapper.class);
	
	public List<Employee> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_employee表中数据");
		List<Employee> list = new ArrayList<Employee>();
		while (rs.next()) {
			Employee employee = new Employee();
			employee.setId(rs.getInt("id"));
			employee.setEno(rs.getString("t_emp_no"));
			employee.setEna(rs.getString("t_emp_na"));
			employee.setEdept(rs.getString("t_emp_dept"));
			employee.setSex(rs.getString("t_sex"));
			employee.setEdu(rs.getString("t_education"));
			employee.setEmail(rs.getString("t_email"));
			employee.setPhone(rs.getString("t_phone"));
			// Date?
			employee.setEtime(rs.getString("t_entry_time"));
			employee.setCtime(rs.getString("t_create_time"));
			list.add(employee);
		}
		logger.debug("EmployeeMapper数据分装成功");
		return list;
	}
}
