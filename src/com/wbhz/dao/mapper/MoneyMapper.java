package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Money;
import com.wbhz.util.RowMapper;

public class MoneyMapper implements RowMapper<Money> {
	private static Logger logger = Logger.getLogger(MoneyMapper.class);
	
	public List<Money> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_money表中数据");
		List<Money> list = new ArrayList<Money>();
		while (rs.next()) {
			Money m = new Money();
			m.setId(rs.getInt("id"));
			m.setMon(rs.getString("t_money_no"));
			m.setMuser(rs.getString("t_money_user"));
//			m.setMtype(rs.getString("t_config_page_value"));
			m.setMtype(rs.getString("t_money_type"));
			m.setMoney(rs.getDouble("t_money_money"));
			m.setCtime(rs.getString("t_money_time"));
			m.setMstatus(rs.getString("t_money_status"));
			m.setMbz(rs.getString("t_money_bz"));
			list.add(m);
		}
		logger.debug("MoneyMapper数据分装成功");
		return list;
	}
}
