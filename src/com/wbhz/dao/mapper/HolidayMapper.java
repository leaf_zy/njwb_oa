package com.wbhz.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.entity.Holiday;
import com.wbhz.util.RowMapper;

public class HolidayMapper implements RowMapper<Holiday> {
	private static Logger logger = Logger.getLogger(HolidayMapper.class);
	
	public List<Holiday> mapperList(ResultSet rs) throws SQLException {
		logger.debug("获得t_holiday表中数据");
		List<Holiday> list = new ArrayList<Holiday>();
		while (rs.next()) {
			Holiday h = new Holiday();
			h.setId(rs.getInt("id"));
			h.setHno(rs.getString("t_holiday_no"));
			h.setHuser(rs.getString("t_holiday_user"));
//			h.setHtype(rs.getString("t_config_page_value"));// ??
			h.setHtype(rs.getString("t_holiday_type"));
			h.setHbz(rs.getString("t_holiday_bz"));
			h.setStime(rs.getString("t_start_time"));
			h.setEtime(rs.getString("t_end_time"));
			h.setHstatus(rs.getString("t_holiday_status"));
			h.setCtime(rs.getString("t_create_time"));
			list.add(h);
		}
		logger.debug("HolidayMapper数据分装成功");
		return list;
	}
}
