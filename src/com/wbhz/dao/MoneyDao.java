package com.wbhz.dao;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Money;

public interface MoneyDao {
	public int insert(Money money) throws SQLException;
	public int delete(Money money) throws SQLException;
	public int update(Money money) throws SQLException;
	public int showInfo(Money money) throws SQLException;
	// 模糊搜索
	public List<Money> getSomeMoney(Money money) throws SQLException;
	public List<Money> getMoneyList(Money money) throws SQLException;
	//获得全部的心情
	List<Money> getAllMoods() throws SQLException; 
	//int currentPage--当前页,int size--每一页显示的条数
	List<Money> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
