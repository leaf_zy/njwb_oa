package com.wbhz.dao;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Employee;

public interface EmployeeDao {
	public int insert(Employee e) throws SQLException;
	public int delete(Employee e) throws SQLException;
	public int update(Employee e) throws SQLException;
	public int showInfoByNo(Employee e) throws SQLException;
	public List<Employee> getEmpListByDept(Employee e) throws SQLException;
	public List<Employee> getEmpListByNo(Employee e) throws SQLException;
	public List<Employee> getEmpListByNoAndName(Employee e) throws SQLException;
	// 模糊搜索
	public List<Employee> selectSomeEmployee(Employee e) throws SQLException;
	//获得全部的心情
	List<Employee> getAllMoods() throws SQLException; 
	//int currentPage--当前页,int size--每一页显示的条数
	List<Employee> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
