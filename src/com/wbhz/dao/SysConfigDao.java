package com.wbhz.dao;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.SysConfig;

public interface SysConfigDao {
	public List<SysConfig> getAllOneSysConfig() throws SQLException;
	
	public List<SysConfig> getAllTwoSysConfig() throws SQLException;
	
	public List<SysConfig> getAllThressSysConfig() throws SQLException;

}
