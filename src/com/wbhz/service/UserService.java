package com.wbhz.service;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.dao.UserDao;
import com.wbhz.entity.User;
import com.wbhz.transaction.Transaction;

public interface UserService {
	public int insert(User user);
	public int update(User user);
	public int delete(User user);
	public List<User> getUserById(int id);
	public List<User> getAllUsersByCondition(User user);
	public List<User> getUserListByRole(User user) throws SQLException;
//	public List<User> getAllUsers();
	public void setUserDao(UserDao dao);//这是重点，通过xml解析获得dao，然后注入到service中 (不一定非得在接口中写，可以只在实现类中写)
	public void setTransaction(Transaction transaction);//这是重点，通过xml解析获得transaction，然后注入到service中 
	public User login(User user);
	public User getUserByLoginName(String loginName);
	// 获得全部的心情
	List<User> getAllMoods() throws SQLException;
	//int currentPage--当前页,int size--每一页显示的条数
	List<User> getMoodsPerPage(int currentPage,int size) throws SQLException;
}
