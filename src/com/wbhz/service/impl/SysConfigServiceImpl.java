package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.SysConfigDao;
import com.wbhz.entity.SysConfig;
import com.wbhz.service.SysConfigService;
import com.wbhz.transaction.Transaction;

public class SysConfigServiceImpl implements SysConfigService {
	private static Logger logger = Logger.getLogger(SysConfigServiceImpl.class);
	private SysConfigDao sysDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setTransaction(Transaction transaction) {
		logger.debug("在SysConfigServiceImpl类中，注入Transaction成功，调用了setTransaction方法");
		this.transaction = transaction;
	}

	public void setSysDao(SysConfigDao sysDao) {
		logger.debug("在SysConfigServiceImpl类中，注入dao成功，调用了setDao方法");
		this.sysDao = sysDao;
	}

	public List<SysConfig> getAllOneSysConfig() throws SQLException {
		// TODO Auto-generated method stub
		return sysDao.getAllOneSysConfig();
	}

	public List<SysConfig> getAllTwoSysConfig() throws SQLException {
		// TODO Auto-generated method stub
		return sysDao.getAllTwoSysConfig();
	}

	public List<SysConfig> getAllThressSysConfig() throws SQLException {
		logger.debug("在SysConfigServiceImpl类中，调用getAllThressSysConfig方法");
		//开始事务
		transaction.begin();
		List<SysConfig> sysList = null;
		try {
			sysList = sysDao.getAllThressSysConfig();
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在UserServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return sysList;
	}


	
}
