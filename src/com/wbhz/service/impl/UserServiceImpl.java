package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.UserDao;
import com.wbhz.entity.User;
import com.wbhz.service.UserService;
import com.wbhz.transaction.Transaction;

public class UserServiceImpl implements UserService {
	//使用log4j的日志记录信息（需要有一个配置文件，确定日志记录的等级，debug，info，warn，error）
	private static Logger logger = Logger.getLogger(UserServiceImpl.class);
	private UserDao userDao = null; 
	private Transaction transaction = null;
	
	public int insert(User user) {
		//开始事务
		transaction.begin();
		logger.debug("在UserServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = userDao.insert(user);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在UserServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}

	public int update(User user) {
		// TODO Auto-generated method stub
		logger.debug("在UserServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = userDao.update(user);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在UserServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}

	public int delete(User user) {
		logger.debug("在UserServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = userDao.delete(user);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在UserServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}

	public List<User> getUserById(int id) {
		logger.debug("在UserServiceImpl类中，调用getUserById方法");
		return userDao.getUserById(id);
	}
	
	public List<User> getAllUsersByCondition(User user) {
		logger.debug("在UserServiceImpl类中，调用getAllUsersByCondition方法");
		return userDao.getAllUsersByCondition(user);
	}

	public List<User> getUserListByRole(User user) throws SQLException {
		logger.debug("在UserServiceImpl类中，调用getUserListByRole方法");
		return userDao.getUserListByRole(user);
	}
//	public List<User> getAllUsers() {
//		// TODO Auto-generated method stub
//		logger.debug("在UserServiceImpl类中，调用getAllUsers方法");
//		return userDao.getAllUsers();
//	}
	// set方法
	public void setUserDao(UserDao userDao) {
		// TODO Auto-generated method stub
		logger.debug("在UserServiceImpl类中，注入dao成功，调用了setDao方法");
		this.userDao=userDao;

	}
//	public void setTransaction(){}
	public void setTransaction(Transaction transaction) {
		// TODO Auto-generated method stub
		logger.debug("在UserServiceImpl类中，注入Transaction成功，调用了setTransaction方法");
		this.transaction=transaction;
	}

	public User login(User user) {
		logger.debug("在UserServiceImpl类中，调用login方法");
		return userDao.login(user);
	}

	public User getUserByLoginName(String loginName) {
		logger.debug("在UserServiceImpl类中，调用getUserByLoginName方法");
		return userDao.getUserByLoginName(loginName);
	}

	public List<User> getAllMoods() throws SQLException {
		logger.debug("在UserServiceImpl类中，调用getAllMoods方法");
		return userDao.getAllMoods();
	}
	
	public List<User> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在UserServiceImpl类中，调用getMoodsPerPage方法");
		return userDao.getMoodsPerPage(currentPage, size);
	}
}
