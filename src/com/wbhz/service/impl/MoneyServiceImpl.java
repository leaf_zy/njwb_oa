package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.MoneyDao;
import com.wbhz.entity.Money;
import com.wbhz.service.MoneyService;
import com.wbhz.transaction.Transaction;

public class MoneyServiceImpl implements MoneyService {
	private static Logger logger = Logger.getLogger(MoneyServiceImpl.class);
	private MoneyDao moneyDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setMoneyDao(MoneyDao moneyDao) {
		this.moneyDao = moneyDao;
	}

	public int insert(Money money) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在MoneyServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = moneyDao.insert(money);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在MoneyServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Money money) {
		logger.debug("在MoneyServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = moneyDao.delete(money);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在MoneyServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Money money) {
		// TODO Auto-generated method stub
		logger.debug("在MoneyServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = moneyDao.update(money);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在MoneyServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int showInfo(Money money) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在EmployeeServiceImpl类中，调用showInfo方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = moneyDao.showInfo(money);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在MoneyServiceImpl类中，showInfo方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	// 模糊搜索
	public List<Money> getSomeMoney(Money money) throws SQLException {
		logger.debug("在MoneyServiceImpl类中，调用getSomeMoney方法");
		return moneyDao.getSomeMoney(money);
	}
	public List<Money> getMoneyList(Money money) throws SQLException {
		logger.debug("在MoneyServiceImpl类中，调用getMoneyList方法");
		return moneyDao.getMoneyList(money);
	}
	
	public List<Money> getAllMoods() throws SQLException {
		logger.debug("在MoneyServiceImpl类中，调用getAllMoods方法");
		return moneyDao.getAllMoods();
	}
	
	public List<Money> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在MoneyServiceImpl类中，调用getMoodsPerPage方法");
		return moneyDao.getMoodsPerPage(currentPage, size);
	}
	
}
