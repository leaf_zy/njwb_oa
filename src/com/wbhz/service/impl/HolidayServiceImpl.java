package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.HolidayDao;
import com.wbhz.entity.Holiday;
import com.wbhz.service.HolidayService;
import com.wbhz.transaction.Transaction;

public class HolidayServiceImpl implements HolidayService {
	private static Logger logger = Logger.getLogger(HolidayServiceImpl.class);
	private HolidayDao holidayDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setHolidayDao(HolidayDao holidayDao) {
		this.holidayDao = holidayDao;
	}

	public int insert(Holiday holiday) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在HolidayServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = holidayDao.insert(holiday);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Holiday holiday) {
		logger.debug("在HolidayServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = holidayDao.delete(holiday);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在HolidayServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Holiday holiday) {
		// TODO Auto-generated method stub
		logger.debug("在HolidayServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = holidayDao.update(holiday);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在HolidayServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int showInfo(Holiday holiday) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在EmployeeServiceImpl类中，调用showInfo方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = holidayDao.showInfo(holiday);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，showInfo方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	// 模糊搜索
	public List<Holiday> getSomeHoliday(String name, String type, String status) throws SQLException {
		logger.debug("在HolidayServiceImpl类中，调用selectSomeEmployee方法");
		return holidayDao.getSomeHoliday(name, type, status);
	}
	public List<Holiday> getHolList(Holiday holiday) throws SQLException {
		logger.debug("在HolidayServiceImpl类中，调用getHolList方法");
		return holidayDao.getHolList(holiday);
	}
	public List<Holiday> getAllMoods() throws SQLException {
		logger.debug("在HolidayServiceImpl类中，调用getAllMoods方法");
		return holidayDao.getAllMoods();
	}
	
	public List<Holiday> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在HolidayServiceImpl类中，调用getMoodsPerPage方法");
		return holidayDao.getMoodsPerPage(currentPage, size);
	}
	
}
