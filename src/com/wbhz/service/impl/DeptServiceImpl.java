package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.DeptDao;
import com.wbhz.entity.Dept;
import com.wbhz.service.DeptService;
import com.wbhz.transaction.Transaction;

public class DeptServiceImpl implements DeptService {
	private static Logger logger = Logger.getLogger(UserServiceImpl.class);
	private DeptDao deptDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setDeptDao(DeptDao deptDao) {
		this.deptDao = deptDao;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public int insertDept(Dept d) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在DeptServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = deptDao.insertDept(d);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在DeptServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Dept d) {
		logger.debug("在DeptServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = deptDao.delete(d);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在DeptServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Dept d) {
		// TODO Auto-generated method stub
		logger.debug("在DeptServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = deptDao.update(d);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在DeptServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int showInfo(Dept d) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在DeptServiceImpl类中，调用showInfo方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = deptDao.showInfo(d);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在DeptServiceImpl类中，showInfo方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public List<Dept> getAllMoods() throws SQLException {
		logger.debug("在DeptServiceImpl类中，调用getAllMoods方法");
		return deptDao.getAllMoods();
	}
	
	public List<Dept> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在DeptServiceImpl类中，调用getMoodsPerPage方法");
		return deptDao.getMoodsPerPage(currentPage, size);
	}
	
}
