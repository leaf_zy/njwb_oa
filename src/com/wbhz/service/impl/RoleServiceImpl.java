package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.RoleDao;
import com.wbhz.entity.Role;
import com.wbhz.service.RoleService;
import com.wbhz.transaction.Transaction;

public class RoleServiceImpl implements RoleService {
	private static Logger logger = Logger.getLogger(RoleServiceImpl.class);
	private RoleDao roleDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public int insert(Role role) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在RoleServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = roleDao.insert(role);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在RoleServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Role role) throws SQLException {
		logger.debug("在RoleServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = roleDao.delete(role);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在RoleServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Role role) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在RoleServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = roleDao.update(role);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在RoleServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public List<Role> getAllMoods() throws SQLException {
		logger.debug("在RoleServiceImpl类中，调用getAllMoods方法");
		return roleDao.getAllMoods();
	}
	
	public List<Role> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在RoleServiceImpl类中，调用getMoodsPerPage方法");
		return roleDao.getMoodsPerPage(currentPage, size);
	}
	
}
