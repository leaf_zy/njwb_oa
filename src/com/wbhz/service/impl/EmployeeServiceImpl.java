package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.EmployeeDao;
import com.wbhz.entity.Employee;
import com.wbhz.service.EmployeeService;
import com.wbhz.transaction.Transaction;

public class EmployeeServiceImpl implements EmployeeService {
	private static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);
	private EmployeeDao employeeDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public int insert(Employee employee) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在EmployeeServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = employeeDao.insert(employee);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Employee employee) {
		logger.debug("在EmployeeServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = employeeDao.delete(employee);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Employee employee) {
		// TODO Auto-generated method stub
		logger.debug("在EmployeeServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = employeeDao.update(employee);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int showInfoByNo(Employee employee) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在EmployeeServiceImpl类中，调用showInfo方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = employeeDao.showInfoByNo(employee);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在EmployeeServiceImpl类中，showInfo方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	public List<Employee> getEmpListByDept(Employee e) throws SQLException {
		logger.debug("在EmployeeServiceImpl类中，调用getEmpListByDept方法");
		return employeeDao.getEmpListByDept(e);
	}
	
	public List<Employee> getEmpListByNo(Employee employee) throws SQLException {
		logger.debug("在EmployeeServiceImpl类中，调用getEmpListByNo方法");
//		//开始事务
//		transaction.begin();
//		List<Employee> empList = null;
//		try {
//			empList = employeeDao.getEmpListByNo(employee);
//			//一旦执行成功就提交
//			transaction.commit();
//		} catch (SQLException e) {
//			logger.warn("在EmployeeServiceImpl类中，getEmpListByNo方法出现异常");
//			//一旦异常就回滚
//			transaction.rollback();
//			e.printStackTrace();
//		}
//		return empList ;
		return employeeDao.getEmpListByNo(employee);
	}
	public List<Employee> getEmpListByNoAndName(Employee e) throws SQLException{
		return employeeDao.getEmpListByNoAndName(e);
	}
	// 模糊搜索
	public List<Employee> selectSomeEmployee(Employee employee) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在EmployeeServiceImpl类中，调用selectSomeEmployee方法");
		return employeeDao.selectSomeEmployee(employee);
		
	}
	
	public List<Employee> getAllMoods() throws SQLException {
		logger.debug("在EmployeeServiceImpl类中，调用getAllMoods方法");
		return employeeDao.getAllMoods();
	}
	
	public List<Employee> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在EmployeeServiceImpl类中，调用getMoodsPerPage方法");
		return employeeDao.getMoodsPerPage(currentPage, size);
	}
	
}
