package com.wbhz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.wbhz.dao.PermissionsDao;
import com.wbhz.entity.Permissions;
import com.wbhz.service.PermissionsService;
import com.wbhz.transaction.Transaction;

public class PermissionsServiceImpl implements PermissionsService {
	private static Logger logger = Logger.getLogger(PermissionsServiceImpl.class);
	private PermissionsDao permissionsDao = null; 
	private Transaction transaction = null;
	
	//set方法内？
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setPermissionsDao(PermissionsDao permissionsDao) {
		this.permissionsDao = permissionsDao;
	}


	public int insert(Permissions permissions) throws SQLException {
		//开始事务
		transaction.begin();
		logger.debug("在PermissionsServiceImpl类中，调用insert方法");
		int count=0;
		try {
			count = permissionsDao.insert(permissions);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在PermissionsServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int delete(Permissions permissions) throws SQLException {
		logger.debug("在PermissionsServiceImpl类中，调用delete方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = permissionsDao.delete(permissions);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在PermissionsServiceImpl类中，insert方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public int update(Permissions permissions) throws SQLException {
		// TODO Auto-generated method stub
		logger.debug("在PermissionsServiceImpl类中，调用update方法");
		//开始事务
		transaction.begin();
		int count=0;
		try {
			count = permissionsDao.update(permissions);
			//一旦执行成功就提交
			transaction.commit();
		} catch (SQLException e) {
			logger.warn("在PermissionsServiceImpl类中，update方法出现异常");
			//一旦异常就回滚
			transaction.rollback();
			e.printStackTrace();
		}
		return count ;
	}
	
	public List<Permissions> getAllMoods() throws SQLException {
		logger.debug("在PermissionsServiceImpl类中，调用getAllMoods方法");
		return permissionsDao.getAllMoods();
	}
	
	public List<Permissions> getMoodsPerPage(int currentPage, int size) throws SQLException {
		logger.debug("在PermissionsServiceImpl类中，调用getMoodsPerPage方法");
		return permissionsDao.getMoodsPerPage(currentPage, size);
	}
	
}
