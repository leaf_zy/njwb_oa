package com.wbhz.service;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Holiday;

public interface HolidayService {
	public int insert(Holiday holiday) throws SQLException;
	public int delete(Holiday holiday);
	public int update(Holiday holiday);
	public int showInfo(Holiday holiday) throws SQLException;
	public List<Holiday> getHolList(Holiday holiday) throws SQLException;
	// 模糊搜索
	public List<Holiday> getSomeHoliday(String name, String type, String status) throws SQLException;
	// 获得全部的心情
	List<Holiday> getAllMoods() throws SQLException;
	//int currentPage--当前页,int size--每一页显示的条数
	List<Holiday> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
