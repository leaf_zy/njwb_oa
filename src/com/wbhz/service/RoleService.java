package com.wbhz.service;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Role;

public interface RoleService {
	public int insert(Role role) throws SQLException;
	public int delete(Role role) throws SQLException;
	public int update(Role role) throws SQLException;
	// 获得全部的心情
	List<Role> getAllMoods() throws SQLException;
	//int currentPage--当前页,int size--每一页显示的条数
	List<Role> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
