package com.wbhz.service;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.dao.SysConfigDao;
import com.wbhz.entity.SysConfig;
import com.wbhz.transaction.Transaction;

public interface SysConfigService {
	public void setSysDao(SysConfigDao sysDao);//这是重点，通过xml解析获得dao，然后注入到service中 (不一定非得在接口中写，可以只在实现类中写)
	public void setTransaction(Transaction transaction);//这是重点，通过xml解析获得transaction，然后注入到service中 
	public List<SysConfig> getAllOneSysConfig() throws SQLException;
	
	public List<SysConfig> getAllTwoSysConfig() throws SQLException;
	
	public List<SysConfig> getAllThressSysConfig() throws SQLException;

}
