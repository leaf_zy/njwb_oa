package com.wbhz.service;

import java.sql.SQLException;
import java.util.List;

import com.wbhz.entity.Permissions;

public interface PermissionsService {
	public int insert(Permissions permissions) throws SQLException;
	public int delete(Permissions permissions) throws SQLException;
	public int update(Permissions permissions) throws SQLException;
	// 获得全部的心情
	List<Permissions> getAllMoods() throws SQLException;
	//int currentPage--当前页,int size--每一页显示的条数
	List<Permissions> getMoodsPerPage(int currentPage,int size) throws SQLException; 
}
